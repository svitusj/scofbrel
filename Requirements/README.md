# README - REQUIREMENT/ORACLE SPECIFICATION #

Place requirement/oracle specification files here. This README documents requirement/oracle specification basics. The concept of oracle, in the context of our work, is applied to code line sets corresponding to the ground truth or full coverage for a requirement. In other words, this code line set represents the most accurate possible solution corresponding a requirement.

### How to add a requirement/oracle specification? ###

* Each requirement (including the corresponding oracle) is specified in an ANSI .txt file

### Format ###

* First line: Name/Label. Example: R-1 (Currently R-1 to R-21 must be specified, unless the tag collection is modified in /src/SCoFBReL.java)
* Second line: [Primary_Keyword] [Secondary_Keyword] [Normal_Keyword_1] ... [NormalKeyword_N]
* Third and subsequent lines: every number corresponding video game source code lines covering the requirement (at least one number is required)
* Last line: ;
* Line comments are allowed, using //

### Example ###

Requirement_0012.txt:  
  
R-12  
// Unlocked jumpgates are used in limbus areas, secondary levels and jumpgates recently unlocked.  
UNLOCKED JUMPGATE used limbus area secondary level recently  
34864  
34865  
38856  
98464  
98466  
98473  
98476  
98585  
103680  
103782  
121384  
;

