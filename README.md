# README - SCoFBReL GENERAL INFORMATION #

SCoFBReL general information file

### What is this repository for? ###

* SCoFBReL prototype

### INSTRUCTIONS ###

* Set up source code files in directories /Requirements and /TestCodeDirectory. See README files in these folders for more information
* Edit ObjectSCoFBReL.java to change the amount of requirement specification/oracle files expected (default 21, from R-1 to R-21)
* Build a runnable .jar file, packaging the required libraries in the generated file.
* Example with 4GB of JVM maximum memory allocation: java -jar -Xmx4g SCoFBReL.jar
* The application generates one CSV file per requirement, taking into account every run for each of them.