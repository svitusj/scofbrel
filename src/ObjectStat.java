public class ObjectStat
{
  /////////////////////////////////////////////////////////////////////////////
  
  //FS = TP + FN
  //MS = TP + FN + FP + TN
  
  String mLabel                  = "";
  
  int    mModelSize              = 0;
  
  int    mFeatureSize            = 0;
  
  int    mNumberOfTruePositives  = 0;
  
  int    mNumberOfFalseNegatives = 0;
  
  int    mNumberOfFalsePositives = 0;
  
  int    mNumberOfTrueNegatives  = 0;
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectStat( String label, int modelSize, int featureSize, int numberOfTruePositives, int numberOfFalseNegatives, int numberOfFalsePositives, int numberOfTrueNegatives )
  {
    mLabel                  = label;
    mModelSize              = modelSize;
    mFeatureSize            = featureSize;
    mNumberOfTruePositives  = numberOfTruePositives;
    mNumberOfFalseNegatives = numberOfFalseNegatives;
    mNumberOfFalsePositives = numberOfFalsePositives;
    mNumberOfTrueNegatives  = numberOfTrueNegatives;
  }
  
  /////////////////////////////////////////////////////////////////////////////
}
