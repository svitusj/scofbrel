public class ObjectDictionaryStore
{
  /////////////////////////////////////////////////////////////////////////////

  public enum LanguageType
  {
    LANGUAGE_TYPE_CPP,
    LANGUAGE_TYPE_JAVA,
    LANGUAGE_TYPES
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectDictionary[] mDictionaries; ///< Dictionary list
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectDictionaryStore()
  {
    Initialize();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void Initialize()
  {
    mDictionaries = new ObjectDictionary[LanguageType.LANGUAGE_TYPES.ordinal()];
    
    // C++ dictionary
    ObjectDictionary dictionaryCPP = new ObjectDictionary();
    dictionaryCPP.add( "alignas" );
    dictionaryCPP.add( "alignof" );
    dictionaryCPP.add( "and" );
    dictionaryCPP.add( "and_eq" );
    dictionaryCPP.add( "asm" );
    dictionaryCPP.add( "auto" );
    dictionaryCPP.add( "bitand" );
    dictionaryCPP.add( "bitor" );
    dictionaryCPP.add( "bool" );
    dictionaryCPP.add( "break" );
    dictionaryCPP.add( "case" );
    dictionaryCPP.add( "catch" );
    dictionaryCPP.add( "char" );
    dictionaryCPP.add( "char16_t" );
    dictionaryCPP.add( "char32_t" );
    dictionaryCPP.add( "class" );
    dictionaryCPP.add( "compl" );
    dictionaryCPP.add( "const" );
    dictionaryCPP.add( "constexpr" );
    dictionaryCPP.add( "const_cast" );
    dictionaryCPP.add( "continue" );
    dictionaryCPP.add( "decltype" );
    dictionaryCPP.add( "default" );
    dictionaryCPP.add( "delete" );
    dictionaryCPP.add( "do" );
    dictionaryCPP.add( "double" );
    dictionaryCPP.add( "dynamic_cast" );
    dictionaryCPP.add( "else" );
    dictionaryCPP.add( "enum" );
    dictionaryCPP.add( "explicit" );
    dictionaryCPP.add( "export" );
    dictionaryCPP.add( "extern" );
    dictionaryCPP.add( "false" );
    dictionaryCPP.add( "float" );
    dictionaryCPP.add( "for" );
    dictionaryCPP.add( "friend" );
    dictionaryCPP.add( "goto" );
    dictionaryCPP.add( "if" );
    dictionaryCPP.add( "inline" );
    dictionaryCPP.add( "int" );
    dictionaryCPP.add( "long" );
    dictionaryCPP.add( "mutable" );
    dictionaryCPP.add( "namespace" );
    dictionaryCPP.add( "new" );
    dictionaryCPP.add( "noexcept" );
    dictionaryCPP.add( "not" );
    dictionaryCPP.add( "not_eq" );
    dictionaryCPP.add( "nullptr" );
    dictionaryCPP.add( "operator" );
    dictionaryCPP.add( "or" );
    dictionaryCPP.add( "or_eq" );
    dictionaryCPP.add( "private" );
    dictionaryCPP.add( "protected" );
    dictionaryCPP.add( "public" );
    dictionaryCPP.add( "register" );
    dictionaryCPP.add( "reinterpret_cast" );
    dictionaryCPP.add( "return" );
    dictionaryCPP.add( "short" );
    dictionaryCPP.add( "signed" );
    dictionaryCPP.add( "sizeof" );
    dictionaryCPP.add( "static" );
    dictionaryCPP.add( "static_assert" );
    dictionaryCPP.add( "static_cast" );
    dictionaryCPP.add( "struct" );
    dictionaryCPP.add( "switch" );
    dictionaryCPP.add( "template" );
    dictionaryCPP.add( "this" );
    dictionaryCPP.add( "thread_local" );
    dictionaryCPP.add( "throw" );
    dictionaryCPP.add( "true" );
    dictionaryCPP.add( "try" );
    dictionaryCPP.add( "typedef" );
    dictionaryCPP.add( "typeid" );
    dictionaryCPP.add( "typename" );
    dictionaryCPP.add( "union" );
    dictionaryCPP.add( "unsigned" );
    dictionaryCPP.add( "using" );
    dictionaryCPP.add( "virtual" );
    dictionaryCPP.add( "void" );
    dictionaryCPP.add( "volatile" );
    dictionaryCPP.add( "wchar_t" );
    dictionaryCPP.add( "while" );
    dictionaryCPP.add( "xor" );
    dictionaryCPP.add( "xor_eq" );
    mDictionaries[LanguageType.LANGUAGE_TYPE_CPP.ordinal()] = dictionaryCPP;
    
    // Java dictionary
    ObjectDictionary dictionaryJava = new ObjectDictionary();
    dictionaryJava.add( "abstract" );
    dictionaryJava.add( "assert" );
    dictionaryJava.add( "boolean" );
    dictionaryJava.add( "break" );
    dictionaryJava.add( "byte" );
    dictionaryJava.add( "case" );
    dictionaryJava.add( "catch" );
    dictionaryJava.add( "char" );
    dictionaryJava.add( "class" );
    dictionaryJava.add( "const" );
    dictionaryJava.add( "continue" );
    dictionaryJava.add( "default" );
    dictionaryJava.add( "do" );
    dictionaryJava.add( "double" );
    dictionaryJava.add( "else" );
    dictionaryJava.add( "enum" );
    dictionaryJava.add( "extends" );
    dictionaryJava.add( "final" );
    dictionaryJava.add( "finally" );
    dictionaryJava.add( "float" );
    dictionaryJava.add( "for" );
    dictionaryJava.add( "goto" );
    dictionaryJava.add( "if" );
    dictionaryJava.add( "implements" );
    dictionaryJava.add( "import" );
    dictionaryJava.add( "instanceof" );
    dictionaryJava.add( "int" );
    dictionaryJava.add( "interface" );
    dictionaryJava.add( "long" );
    dictionaryJava.add( "native" );
    dictionaryJava.add( "new" );
    dictionaryJava.add( "package" );
    dictionaryJava.add( "private" );
    dictionaryJava.add( "protected" );
    dictionaryJava.add( "public" );
    dictionaryJava.add( "return" );
    dictionaryJava.add( "short" );
    dictionaryJava.add( "static" );
    dictionaryJava.add( "strictfp" );
    dictionaryJava.add( "super" );
    dictionaryJava.add( "switch" );
    dictionaryJava.add( "synchronized" );
    dictionaryJava.add( "this" );
    dictionaryJava.add( "throw" );
    dictionaryJava.add( "throws" );
    dictionaryJava.add( "transient" );
    dictionaryJava.add( "try" );
    dictionaryJava.add( "void" );
    dictionaryJava.add( "volatile" );
    dictionaryJava.add( "while" );
    mDictionaries[LanguageType.LANGUAGE_TYPE_JAVA.ordinal()] = dictionaryJava;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public boolean IsIndependentFromDictionary( String word, LanguageType languageType )
  {
    if ( languageType.ordinal() < LanguageType.LANGUAGE_TYPES.ordinal() ) {
      return !mDictionaries[languageType.ordinal()].contains( word );
    }
    else {
      return true;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
}