// Array lists required
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Random;
import core.utils.EAUtils;

public class ObjectPopulationStore
{
  /////////////////////////////////////////////////////////////////////////////
  
  public static final int     CONSTANT_INITIAL_POPULATION_SIZE      = 1000;
  
  public static final float   CONSTANT_MUTATION_PROBABILITY_DEFAULT = 0.25f;
  
  public static final boolean CONSTANT_IS_CUSTOM_WHEEL_SELECTION_USED = true;
  
  /////////////////////////////////////////////////////////////////////////////e
  
  private ArrayList< ObjectCodeLineSimplified > mSimplifiedCodeLines;               ///< Filtered code lines
  
  private Set< Integer >                        mSimplifiedCodeLineMutationIndexes; ///< Simplified line indexes used in driven mutations
  
  private ObjectCodeFragment                    mCodeFragmentCompleteSourceCode;    ///< Special code fragment containing the complete source code
  
  private int                                   mContiguousCodeFragmentsLineIndex;  ///< Contiguous code fragment line index
  
  private boolean                               mIsCodeFragmentAFunction;           ///< True if code fragments are functions
  
  private ArrayList< ObjectCodeFragment >       mCodeFragments;                     ///< Code fragment list

  /////////////////////////////////////////////////////////////////////////////

  public ObjectPopulationStore()
  {
    mSimplifiedCodeLines = new ArrayList< ObjectCodeLineSimplified >();
    mSimplifiedCodeLineMutationIndexes = new HashSet< Integer >();
    mCodeFragmentCompleteSourceCode = null;
    mCodeFragments = new ArrayList< ObjectCodeFragment >();
    mContiguousCodeFragmentsLineIndex = 0;
    mIsCodeFragmentAFunction = false;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public boolean IsCodeFragmentAFunction()
  {
    return mIsCodeFragmentAFunction;
  }

  /////////////////////////////////////////////////////////////////////////////
  
  public void SetIsCodeFragmentAFunction( boolean isCodeFragmentAFunction )
  {
    mIsCodeFragmentAFunction = isCodeFragmentAFunction;
  }

  /////////////////////////////////////////////////////////////////////////////

  public int GetNumberOfLinesStored()
  {
    return mSimplifiedCodeLines.size();
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public boolean IsContiguousCodeFragmentGenerationStillPossible()
  {
    return ( mContiguousCodeFragmentsLineIndex + 1 ) < mSimplifiedCodeLines.size();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void AddContent( ArrayList< ObjectFileData > files )
  {
    ArrayList< ObjectCodeLine > lines = new ArrayList< ObjectCodeLine >();
    
    for ( int i = 0; i < files.size(); ++i ) {
      lines =  files.get( i ).GetLines();
      for ( int j = 0; j < lines.size(); ++j ) {
        mSimplifiedCodeLines.add( new ObjectCodeLineSimplified( i, j, lines.get( j ).GetLineType() ) );
      }
    }
    if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE ) {
      System.out.println( "Total files: " + files.size() );
      System.out.println( "Total code lines: " + mSimplifiedCodeLines.size() );
    }
    CreateCodeFragmentCompleteSourceCode();
    
    mContiguousCodeFragmentsLineIndex = 0;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void AddSimplifiedCodeLineMutationIndex( int simplifiedLineIndex )
  {
    mSimplifiedCodeLineMutationIndexes.add( simplifiedLineIndex );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ArrayList< Integer > GetRandomSimplifiedCodeLineMutationIndexes()
  {
    ArrayList< Integer > indexList = new ArrayList< Integer >();
    
    int index = new Random( System.nanoTime() ).nextInt( mSimplifiedCodeLineMutationIndexes.size() > 1 ? mSimplifiedCodeLineMutationIndexes.size() : mSimplifiedCodeLines.size() );
    Iterator< Integer > iter = mSimplifiedCodeLineMutationIndexes.iterator();
    if ( mSimplifiedCodeLineMutationIndexes.size() > 1 ) {
      for ( int i = 0; i < index; i++ ) {
        iter.next();
      }
    }
    int startingIndex = mSimplifiedCodeLineMutationIndexes.size() > 1 ? iter.next() : index;
    
    if ( ObjectSCoFBReL.CONSTANT_IS_METHOD_GRANULARITY_USED ) {
      // No size limit, just complete method line selection
      boolean isPreSearchActive = true;
      boolean isPostSearchActive = true;
      for ( int j = startingIndex - 1; j >= 0 && isPreSearchActive; --j ) {
        indexList.add( j );
        if ( mSimplifiedCodeLines.get( j ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_STARTER ||
             mSimplifiedCodeLines.get( j ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_SEPARATOR ) {
          isPreSearchActive = false;
        }
      }
      for ( int k = startingIndex; k < mSimplifiedCodeLines.size() && isPostSearchActive; ++k ) {
        indexList.add( k );
        if ( mSimplifiedCodeLines.get( k ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_STARTER ||
            mSimplifiedCodeLines.get( k ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_SEPARATOR ) {
         isPostSearchActive = false;
       }
      }
    }
    else {
      // Default line compilation with a certain size
      for ( int m = startingIndex; m < mSimplifiedCodeLines.size() && ( m - startingIndex ) < ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_DEFAULT_SIZE; ++m ) {
        indexList.add( m );
      }
    }
    
    return indexList;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectCodeFragment GetCodeFragment( int codeFragmentIndex )
  {
    try {
      return mCodeFragments.get( codeFragmentIndex );
    }
    catch( Exception exception ) {
      System.out.println( "Exception accessing code fragment " + codeFragmentIndex + ": " + exception );
      return null;
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectCodeFragment CreateCodeFragment( int maximumNumberOfLines, int optionalStartLineIndex )
  {
    // USAGE:
    // optionalStartLineIndex < 0 -> Random start line
    // maximumNumberOfLines < 0 -> Lines above and below the start line are added to select a complete method
    
    ObjectCodeFragment codeFragment = new ObjectCodeFragment();
    boolean isInvalid = false;
    if ( maximumNumberOfLines < 0 ) {
      maximumNumberOfLines = mSimplifiedCodeLines.size() + 1;
    }
    
    Random random = new Random( System.nanoTime() );
    int indexStart =
        IsCodeFragmentAFunction() ?
            mContiguousCodeFragmentsLineIndex :
            ( maximumNumberOfLines == mSimplifiedCodeLines.size() ? 0 : ( ( optionalStartLineIndex >= 0 ) ? optionalStartLineIndex : random.nextInt( mSimplifiedCodeLines.size() ) ) );
    int numberOfLinesAdded = 0;
    boolean isWrappedAround = false;
    
    for ( int i = indexStart;
        IsCodeFragmentAFunction() ?
            ( i < mSimplifiedCodeLines.size() ) :
            ( numberOfLinesAdded < maximumNumberOfLines);
          ++i ) {
      codeFragment.add( isWrappedAround ? i : ( codeFragment.isEmpty() ? 0 : codeFragment.size() ), new ObjectCodeFragmentElement( mSimplifiedCodeLines.get( i ).first, mSimplifiedCodeLines.get( i ).second, i ) );
      ++numberOfLinesAdded;
      ++mContiguousCodeFragmentsLineIndex;
      if ( IsCodeFragmentAFunction() &&
        mSimplifiedCodeLines.get( i ).third == ObjectCodeLine.LineType.LINE_TYPE_CLASS_DECLARATION ) {
        isInvalid = true;
      }
      if ( ( IsCodeFragmentAFunction() || maximumNumberOfLines > mSimplifiedCodeLines.size() ) &&
          ( ( ( i + 1 ) < mSimplifiedCodeLines.size() && mSimplifiedCodeLines.get( i + 1 ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_STARTER ) ||
           mSimplifiedCodeLines.get( i ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_SEPARATOR ) ){
        // Function starter or separator found
        
        // Time to search for another separator in the opposite direction
        if ( maximumNumberOfLines > mSimplifiedCodeLines.size() ) {
          boolean isLastLineAddedNotSeparator = true;
          for ( int h = indexStart - 1; h >= 0 && isLastLineAddedNotSeparator; --h ) {
            codeFragment.add( 0, new ObjectCodeFragmentElement( mSimplifiedCodeLines.get( h ).first, mSimplifiedCodeLines.get( h ).second, h ) );
            ++numberOfLinesAdded;
            if ( ( mSimplifiedCodeLines.get( h ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_STARTER ) ||
                 ( mSimplifiedCodeLines.get( h ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_SEPARATOR ) ) {
              break;
            }
          }
        }
        
        break;
      }
      
      
      if ( ( i + 1 ) >= mSimplifiedCodeLines.size() ) {
        break;
      }
    }
    
    int numberOfLinesPending = maximumNumberOfLines > mSimplifiedCodeLines.size() ? 0 : ( maximumNumberOfLines - numberOfLinesAdded );
    if ( !IsCodeFragmentAFunction() && numberOfLinesPending > 0 ) {
      for ( int j = 0; j < numberOfLinesPending; ++j ) {
        // TODO: NULL element
        codeFragment.add( codeFragment.isEmpty() ? 0 : codeFragment.size(), new ObjectCodeFragmentElement( ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID, ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID, ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID ) );
      }
    }

    
    if ( isInvalid ) {
      codeFragment.clear();
    }
      
    return codeFragment;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectCodeFragment SetAndGetCodeFragmentFitToMethodVersion( int codeFragmentIndex )
  {
    ObjectCodeFragment codeFragment = GetCodeFragment( codeFragmentIndex );
    ArrayList< ObjectPair< Integer, Boolean > > startLineIndices = new ArrayList< ObjectPair< Integer, Boolean > >();
    
    for ( int i = 0; i < codeFragment.size(); ++i ) {
      if ( i == 0 ) {
        startLineIndices.add( new ObjectPair< Integer, Boolean >( codeFragment.get( i ).third - 1, false ) );
      }
      else if ( ( i + 1 ) == codeFragment.size() ) {
        startLineIndices.add( new ObjectPair< Integer, Boolean >( codeFragment.get( i ).third + 1, true ) );
      }
      else if ( ( codeFragment.get( i ).third + 1 ) != codeFragment.get( i + 1 ).third ) {
        // Gap detected
        startLineIndices.add( new ObjectPair< Integer, Boolean >( codeFragment.get( i ).third + 1, true ) );
        startLineIndices.add( new ObjectPair< Integer, Boolean >( codeFragment.get( i + 1 ).third - 1, false ) );
      }
    }
    
    boolean isAdditionEnabled = true;
    for ( int j = 0; j < startLineIndices.size(); ++j ) {
      isAdditionEnabled = true;
      int globalLineIndex = startLineIndices.get( j ).first;
      boolean isForwardAddition = startLineIndices.get( j ).second;
      
      for ( int k = globalLineIndex; isAdditionEnabled && k >= 0 && k < mSimplifiedCodeLines.size(); k = isForwardAddition ? ( k + 1 ) : ( k - 1 ) ) {
        if ( /*mSimplifiedCodeLines.get( k ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_STARTER ||
             mSimplifiedCodeLines.get( k ).third == ObjectCodeLine.LineType.LINE_TYPE_FUNCTION_SEPARATOR*/
             mSimplifiedCodeLines.get( k ).third != ObjectCodeLine.LineType.LINE_TYPE_NORMAL ) {
          isAdditionEnabled = false;
        }
        else {
          codeFragment.add( 
            new ObjectCodeFragmentElement(
              mSimplifiedCodeLines.get( k ).first,
              mSimplifiedCodeLines.get( k ).second,
              globalLineIndex ) );
        }
      }
     
    }
    
    codeFragment.Sort();
    mCodeFragments.set( codeFragmentIndex, codeFragment );
    return codeFragment;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void CreateCodeFragmentCompleteSourceCode()
  {
    mCodeFragmentCompleteSourceCode = CreateCodeFragment( mSimplifiedCodeLines.size(), -1 );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectCodeFragment GetCodeFragmentCompleteSourceCode()
  {
    return mCodeFragmentCompleteSourceCode;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public int AddCodeFragmentAndReturnIndex( ObjectCodeFragment codeFragment )
  {
    mCodeFragments.add( codeFragment );
    return mCodeFragments.size() - 1;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void RemoveCodeFragment( int codeFragmentIndex )
  {
    if ( mCodeFragments.size() > codeFragmentIndex ) {
      mCodeFragments.remove( codeFragmentIndex );
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public boolean MutateAndGetSuccess( int codeFragmentIndex, float mutationProbability )
  {
    boolean isMutationDone = false;
    if ( codeFragmentIndex < mCodeFragments.size() ) {
      Random random = new Random( System.nanoTime() );
      int randomIntervalExclusiveUpperLimit = ( int )( 1.0f / ObjectUtils.GetClamped( mutationProbability, 0.0f, 1.0f ) );
      ObjectCodeFragment codeFragment = mCodeFragments.get( codeFragmentIndex );
      
      if ( random.nextInt( 4 ) == 0 ) {
        // Subtractive mutation
        int codeFragmentInitialSize = codeFragment.size();
        if ( codeFragmentInitialSize > 1 ) {
          if ( ObjectSCoFBReL.CONSTANT_IS_MUTATION_DRIVEN ) {
            
            
            int currentFileIndex = -1;
            int numberOfFileIndices = 0;
            for ( int h = 0; h < codeFragment.size(); ++h ) {
              if ( codeFragment.get( h ).first != currentFileIndex ) {
                currentFileIndex = codeFragment.get( h ).first;
                ++numberOfFileIndices;                
              }
            }
            

              // Code line granularity
            
              currentFileIndex = -1;
              boolean isFileLineRemovalEnabled = false;
              for ( int i = codeFragmentInitialSize - 1; i >= 0; --i ) {
                if ( codeFragment.get( i ).first != currentFileIndex ) {
                  isFileLineRemovalEnabled = false;
                  currentFileIndex = codeFragment.get( i ).first;
                  if ( numberOfFileIndices > 1 && random.nextBoolean() ) {
                    --numberOfFileIndices;
                    isFileLineRemovalEnabled = true;
                  }
                }
                if ( isFileLineRemovalEnabled && !codeFragment.isEmpty() ) {
                  codeFragment.remove( i );
                  isMutationDone = true;
                }
              }
            
            
            
          }
          else {
            for ( int i = codeFragmentInitialSize - 1; i >= 0; --i ) {
              if ( random.nextInt( randomIntervalExclusiveUpperLimit ) == 0 ) {
                // TODO: NULL element
                codeFragment.get( i ).first = ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID;
                codeFragment.get( i ).second = ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID;
                codeFragment.get( i ).third = ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID;
                isMutationDone = true;
              }
            }
          }
        }
      }
      else {
        // Additive mutation
        if ( !codeFragment.isEmpty() ) {
          boolean isExpansionDetected = false;
          if ( ObjectSCoFBReL.CONSTANT_IS_MUTATION_DRIVEN ) {
            ArrayList< Integer > newSimplifiedCodeLineIndexes = GetRandomSimplifiedCodeLineMutationIndexes();
            for ( int k = 0; k < newSimplifiedCodeLineIndexes.size(); ++k ) {
              ObjectCodeLineSimplified mutationDrivenSimplifiedCodeLine = mSimplifiedCodeLines.get( newSimplifiedCodeLineIndexes.get( k ) );
              isExpansionDetected |= ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_LAST_ADDITION_INDEX_INVALID != codeFragment.AddLineFromFileAndReturnIndex( mutationDrivenSimplifiedCodeLine.first, mutationDrivenSimplifiedCodeLine.second, newSimplifiedCodeLineIndexes.get( k ) );
            }
          }
          else {
            int previousNewLineIndex = codeFragment.get( 0 ).third - 1;
            int followingNewLineIndex = codeFragment.get( codeFragment.size() - 1 ).third + 1;
            int expansionRange = codeFragment.size() / 2;
            ObjectCodeLineSimplified simplifiedCodeLine = new ObjectCodeLineSimplified( 0, 0, ObjectCodeLine.LineType.LINE_TYPE_NORMAL );
            for ( int j = 0;
                  j < expansionRange &&
                  previousNewLineIndex >= 0 && previousNewLineIndex < mSimplifiedCodeLines.size() &&
                  followingNewLineIndex >= 0 && followingNewLineIndex < mSimplifiedCodeLines.size();
                  ++j ) {
              if ( random.nextInt( randomIntervalExclusiveUpperLimit ) == 0 ) {
               simplifiedCodeLine = mSimplifiedCodeLines.get( previousNewLineIndex );
               isExpansionDetected |= ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_LAST_ADDITION_INDEX_INVALID != codeFragment.AddLineFromFileAndReturnIndex( simplifiedCodeLine.first, simplifiedCodeLine.second, previousNewLineIndex );
              }
              --previousNewLineIndex;
              if ( random.nextInt( randomIntervalExclusiveUpperLimit ) == 0 ) {
                simplifiedCodeLine = mSimplifiedCodeLines.get( followingNewLineIndex );
                isExpansionDetected |= ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_LAST_ADDITION_INDEX_INVALID != codeFragment.AddLineFromFileAndReturnIndex( simplifiedCodeLine.first, simplifiedCodeLine.second, followingNewLineIndex );
              }
              ++followingNewLineIndex;
            }
          }
          if ( isExpansionDetected ) {
            isMutationDone = true;
            codeFragment.Sort();
          }
        }
      }
    }
    
    return isMutationDone;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectPair< Integer, Integer > CrossoverAddAndGetNewIndices(
                     int firstCodeFragmentIndex, int secondCodeFragmentIndex )
  {
    ObjectPair< Integer, Integer > newCodeFragmentIndices =
        new ObjectPair< Integer, Integer >(
            ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_INDEX_INVALID,
            ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_INDEX_INVALID ); 
    ObjectCodeFragment firstCodeFragment = new ObjectCodeFragment();
    ObjectCodeFragment secondCodeFragment = new ObjectCodeFragment();
    ObjectCodeFragment firstNewCodeFragment = new ObjectCodeFragment();
    ObjectCodeFragment secondNewCodeFragment = new ObjectCodeFragment();
    
    if ( firstCodeFragmentIndex < mCodeFragments.size() ) {
      firstCodeFragment = mCodeFragments.get( firstCodeFragmentIndex );
    }
    if ( secondCodeFragmentIndex < mCodeFragments.size() ) {
      secondCodeFragment = mCodeFragments.get( secondCodeFragmentIndex );
    }
    
    boolean isExpansionDetected = false;
    Random random = new Random( System.nanoTime() );
    int numberOfLinesInFirstHalf = ObjectCodeFragment.CONSTANT_IS_NEO_CROSSOVER_ENABLED ? firstCodeFragment.size() : random.nextInt( firstCodeFragment.size() + 1 );
    
    
    // First code fragment creation
    for ( int i = 0; i < numberOfLinesInFirstHalf; ++i ) {
      firstNewCodeFragment.add( new ObjectCodeFragmentElement( firstCodeFragment.get( i ) ) );
      isExpansionDetected = true;
    }
    for ( int j = ObjectCodeFragment.CONSTANT_IS_NEO_CROSSOVER_ENABLED ? 0 : numberOfLinesInFirstHalf; j < secondCodeFragment.size(); ++j ) {
      firstNewCodeFragment.add( new ObjectCodeFragmentElement( secondCodeFragment.get( j ) ) );
      isExpansionDetected = true;
    }
    newCodeFragmentIndices.first = AddCodeFragmentAndReturnIndex( firstNewCodeFragment );
    
    
    // Second code fragment creation
    if ( !ObjectCodeFragment.CONSTANT_IS_NEO_CROSSOVER_ENABLED ) {
      for ( int ii = 0; ii < numberOfLinesInFirstHalf && ii < secondCodeFragment.size(); ++ii ) {
        secondNewCodeFragment.add( new ObjectCodeFragmentElement( secondCodeFragment.get( ii ) ) );
        isExpansionDetected = true;
      }
      for ( int jj = ObjectCodeFragment.CONSTANT_IS_NEO_CROSSOVER_ENABLED ? 0 : numberOfLinesInFirstHalf; jj < firstCodeFragment.size(); ++jj ) {
        secondNewCodeFragment.add( new ObjectCodeFragmentElement( firstCodeFragment.get( jj ) ) );
        isExpansionDetected = true;
      }
    }
    newCodeFragmentIndices.second = ObjectCodeFragment.CONSTANT_IS_NEO_CROSSOVER_ENABLED ? ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_INDEX_INVALID : AddCodeFragmentAndReturnIndex( secondNewCodeFragment );
    
    
    // New fragments are inspected in order to keep global source code line order
    if ( isExpansionDetected ) {
      firstNewCodeFragment.Sort();
      secondNewCodeFragment.Sort();
    }
    
    return newCodeFragmentIndices;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public Integer FuseAndGetNewIndex( ArrayList< Integer > parentCodeFragmentIndices )
  {
    ObjectCodeFragment parentCodeFragment = null;
    ObjectCodeFragment newCodeFragment = new ObjectCodeFragment();
    for ( int i = 0; i < parentCodeFragmentIndices.size(); ++i ) {
      parentCodeFragment = null;
      if ( parentCodeFragmentIndices.get( i ) < mCodeFragments.size() ) {
        parentCodeFragment = mCodeFragments.get( parentCodeFragmentIndices.get( i ) );
      }
      for ( int j = 0; j < parentCodeFragment.size(); ++j ) {
        newCodeFragment.AddLineFromFileAndReturnIndex( parentCodeFragment.get( j ).first, parentCodeFragment.get( j ).second, parentCodeFragment.get( j ).third );
      }
    }
    newCodeFragment.Sort();
    return AddCodeFragmentAndReturnIndex( newCodeFragment );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< Integer > GetFittestWheel( ArrayList< ObjectPair< Integer, Double > > codeFragmentIndexFitnessValues, int selectionSize )
  {
    //Table for accumulated fitness values
    double[] cumulativeFitnesses = new double[codeFragmentIndexFitnessValues.size()];
    cumulativeFitnesses[0] = codeFragmentIndexFitnessValues.get(0).second;

    //Fill the accumulated table
    for ( int i = 1; i < codeFragmentIndexFitnessValues.size(); i++ )
    {
        double fitness = codeFragmentIndexFitnessValues.get( i ).second;
        cumulativeFitnesses[i] = cumulativeFitnesses[i - 1] + fitness;
    }
   
    ArrayList< Integer > selection = new ArrayList< Integer >( selectionSize );
    ArrayList< Double > selectionValues = new ArrayList< Double >( selectionSize );
    for ( int i = 0; i < selectionSize; i++ ) {
        double randomFitness = /*random.nextDouble()*/EAUtils.getRandomDouble() * cumulativeFitnesses[cumulativeFitnesses.length - 1];
        int index = Arrays.binarySearch( cumulativeFitnesses, randomFitness );
        //binarySearch returns -insertionPoint - 1 when the exact value is not found
        if (index < 0) {   
          //correct the index to know how was selected.
          index = Math.abs(index + 1);
        }
        
        if ( index < 0 || index >= codeFragmentIndexFitnessValues.size() ) {
          index = EAUtils.getRandomInt( codeFragmentIndexFitnessValues.size() );
        }
        selection.add( codeFragmentIndexFitnessValues.get( index ).first );
        selectionValues.add( codeFragmentIndexFitnessValues.get( index ).second );
    }
    
    if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE ) {
      System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + "Fittest from " + codeFragmentIndexFitnessValues.size() + " code fragments" );
      for ( int i = 0; i < selection.size(); ++i ) {
        System.out.println( "Code Fragment: " + selection.get( i ) + "  Value: " +  selectionValues.get( i ) );
      }
    }
    
    return selection;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ArrayList< Integer >  GetFittestAlternate( ArrayList< ObjectPair< Integer, Double > > codeFragmentIndexFitnessValues, int selectionSize )
  {
    Collections.sort(codeFragmentIndexFitnessValues, new Comparator<ObjectPair< Integer, Double >>() {

      @Override
      public int compare(ObjectPair< Integer, Double > first, ObjectPair< Integer, Double > second) {
        
        return -Double.compare(first.second, second.second);
      }
    });

    ArrayList< Integer > selection = new ArrayList< Integer >( selectionSize );
    for ( int i = 0; selection.size() < selectionSize && i < codeFragmentIndexFitnessValues.size(); ++i ) {
      if ( codeFragmentIndexFitnessValues.get( i ).second >= 0.0f && codeFragmentIndexFitnessValues.get( i ).second <= 1.0f ) {
          selection.add( codeFragmentIndexFitnessValues.get( i ).first );
      }
    }
    
    if ( selection.size() < selectionSize ) {
      System.out.println( "Selection size error. Expected: " + selectionSize + ". Obtained: " + selection.size() );
    }
    return selection;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ArrayList< Integer >  GetFittest( ArrayList< ObjectPair< Integer, Double > > codeFragmentIndexFitnessValues, int selectionSize )
  {
    if ( CONSTANT_IS_CUSTOM_WHEEL_SELECTION_USED ) {
      return  GetFittestAlternate( codeFragmentIndexFitnessValues, selectionSize );
    }
    else {
      return  GetFittestWheel( codeFragmentIndexFitnessValues, selectionSize );
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ArrayList< Integer >  GetWorst( ArrayList< ObjectPair< Integer, Double > > codeFragmentIndexFitnessValues, int selectionSize )
  {
    Collections.sort(codeFragmentIndexFitnessValues, new Comparator<ObjectPair< Integer, Double >>() {

      @Override
      public int compare(ObjectPair< Integer, Double > first, ObjectPair< Integer, Double > second) {
        
        return -Double.compare(first.second, second.second);
      }
    });

    ArrayList< Integer > selection = new ArrayList< Integer >( selectionSize );
    for ( int i = codeFragmentIndexFitnessValues.size() - 1; selection.size() < selectionSize && i >= 0; --i ) {
      selection.add( codeFragmentIndexFitnessValues.get( i ).first );
    }
    
    if ( selection.size() < selectionSize ) {
      System.out.println( "Selection size error. Expected: " + selectionSize + ". Obtained: " + selection.size() );
    }
    return selection;
  }
  
  /////////////////////////////////////////////////////////////////////////////

}


