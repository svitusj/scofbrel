public class ObjectCodeLineSimplified extends ObjectTrio< Integer, Integer, ObjectCodeLine.LineType >
{
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectCodeLineSimplified( int fileIndex, int localLineIndex, ObjectCodeLine.LineType lineType )
  {
    super( fileIndex, localLineIndex, lineType );
  }
  
  /////////////////////////////////////////////////////////////////////////////
}