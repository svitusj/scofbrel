package oracle;

import java.util.List;

public abstract class BaseOracle<T> {

	abstract List<T> getTestCases();
}
