package oracle;

import org.eclipse.emf.ecore.EObject;

import core.Query;
import core.individual.IIndividual;

public class TestCaseEMFText {

	private Query query;
	private EObject model;
	private IIndividual solution;
	private float score;
	
	
	
	public Query getQuery() {
		return query;
	}



	public void setQuery(Query query) {
		this.query = query;
	}



	public EObject getModel() {
		return model;
	}



	public void setModel(EObject model) {
		this.model = model;
	}



	public IIndividual getSolution() {
		return solution;
	}



	public void setSolution(IIndividual solution) {
		this.solution = solution;
	}



	public float getScore() {
		return score;
	}



	public void setScore(float score) {
		this.score = score;
	}



	public TestCaseEMFText(Query query, EObject model, IIndividual solution) {
		super();
		this.query = query;
		this.model = model;
		this.solution = solution;
	}
	
	
}
