package oracle;

import java.util.List;

public abstract class OracleEMFText extends BaseOracle<TestCaseEMFText> {

	abstract List<TestCaseEMFText> getTestCases();
	
}
