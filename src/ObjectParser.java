// File management required
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;

// List management required
import java.util.ArrayList;

public class ObjectParser
{
  
  /////////////////////////////////////////////////////////////////////////////
  
  public static final boolean CONSTANT_IS_INPUT_STREAM_USED  = false;
  
  public static final String  CONSTANT_INPUT_STREAM_ENCODING = "utf8";
  
  public static final String  CONSTANT_NEW_LINE_STRING       = "\r\n";
  
  public static final String  CONSTANT_VERBOSE_NEW_FILE_LINE = ObjectParser.CONSTANT_NEW_LINE_STRING + "//////////////////////////////" + ObjectParser.CONSTANT_NEW_LINE_STRING;
  
  /////////////////////////////////////////////////////////////////////////////
  
  private ArrayList< String > mSkippedFileExtensions;    ///< Skipped file extensions
  
  private String              mOptionDirectoryName;      ///< Current option directory name

  private String              mSourceCodeDirectoryName;  ///< Current source code directory name
  
  private String              mRequirementDirectoryName; ///< Current requirement directory name
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectParser()
  {
    mOptionDirectoryName = "";
    mSourceCodeDirectoryName = "";
    mRequirementDirectoryName = "";
    mSkippedFileExtensions = new ArrayList< String >();
    mSkippedFileExtensions.add( ".md" );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void SetOptionDirectoryName( String currentDirectoryName )
  {
    mOptionDirectoryName = currentDirectoryName;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< ObjectFileData > ParseOptionDirectory()
  {
    return ParseDirectory( new File( mOptionDirectoryName ) );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void SetSourceCodeDirectoryName( String currentDirectoryName )
  {
    mSourceCodeDirectoryName = currentDirectoryName;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< ObjectFileData > ParseSourceCodeDirectory()
  {
    return ParseDirectory( new File( mSourceCodeDirectoryName ) );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void SetRequirementDirectoryName( String currentDirectoryName )
  {
    mRequirementDirectoryName = currentDirectoryName;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< ObjectFileData > ParseRequirementDirectory()
  {
    return ParseDirectory( new File( mRequirementDirectoryName ) );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ArrayList< ObjectFileData > ParseDirectory( File directory )
  {
    ArrayList< ObjectFileData > files = new ArrayList< ObjectFileData >();
    
    File fileList[] = directory.listFiles();
    if ( fileList != null ) {
      
      ObjectFileData objectFileData = new ObjectFileData();
      ArrayList< ObjectCodeLine > lines = new ArrayList< ObjectCodeLine >();
      ObjectCodeLine line = new ObjectCodeLine( 0, 0, "", new ObjectWordSet() );
      String stringFormat = "";
      
      for ( int i = 0; i < fileList.length; ++i ) {
        if ( fileList[i].isDirectory() ) {
          files.addAll( ParseDirectory( fileList[i] ) );
        }
        else if ( IsValidFile( fileList[i].getName() ) ) {
          files.add( ParseFile( fileList[i] ) );
          
          if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE && ObjectSCoFBReL.CONSTANT_IS_FILE_PARSING_SHOWN ) {          
            objectFileData = files.get( files.size() - 1 );
            System.out.println( "File name: " + objectFileData.GetFileName() + ObjectParser.CONSTANT_NEW_LINE_STRING );
            lines = objectFileData.GetLines();
            stringFormat = "%0" + ( ( int )Math.log10( lines.size() ) + 1 ) + "d";
            for ( int k = 0; k < lines.size(); ++k ) {
              line = lines.get( k );
              System.out.println( "File Line " + String.format( stringFormat, line.second ) + " : " + line.third );
            }
          }
          
        }
      }
    }

    return files;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectFileData ParseFile( File file )
  {
    if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE && ObjectSCoFBReL.CONSTANT_IS_FILE_PARSING_SHOWN ) {
      System.out.println(
          CONSTANT_VERBOSE_NEW_FILE_LINE + 
          "Opening file: " + file.getPath() +
          CONSTANT_VERBOSE_NEW_FILE_LINE );
    }
    if ( CONSTANT_IS_INPUT_STREAM_USED ) {
      return ParseFileStream( file );
    }
    else {
      return ParseFileSimple( file );
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectFileData ParseFileSimple( File file )
  {
    ObjectFileData objectFileData = new ObjectFileData();
    
    try {
      FileReader fileReader = new FileReader( file.getPath() );
      BufferedReader bufferedReader = new BufferedReader( fileReader );
      
      String line;
      objectFileData.SetFileName( file.getPath() );
      while ( ( line = bufferedReader.readLine() ) != null ) {
        objectFileData.AddLine( line );
      }
      
      fileReader.close();
    }
    catch( Exception exception ) {
      System.out.println( "Exception reading file " + file.getPath() + ": " + exception );
    }
    
    return objectFileData;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectFileData ParseFileStream( File file )
  {
    ObjectFileData objectFileData = new ObjectFileData();
    
    try {
      FileInputStream fileInputStream = new FileInputStream( file );
      InputStreamReader inputStreamReader = new InputStreamReader( fileInputStream, CONSTANT_INPUT_STREAM_ENCODING );
      BufferedReader bufferedReader = new BufferedReader( inputStreamReader );
      
      String line;
      objectFileData.SetFileName( file.getPath() );
      while ( ( line = bufferedReader.readLine() ) != null ) {
        objectFileData.AddLine( line );
      }
      
      fileInputStream.close();
    }
    catch( Exception exception ) {
      System.out.println( "Exception reading file " + file.getPath() + ": " + exception );
    }
    
    return objectFileData;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  private boolean IsValidFile( String fileName )
  {
    boolean isValidFile = true;
    for ( int i = 0; isValidFile && i < mSkippedFileExtensions.size(); ++i ) {
      if ( fileName.endsWith( mSkippedFileExtensions.get( i ) ) ) {
        isValidFile = false;
      }
    }
    return isValidFile;
  }
  
  /////////////////////////////////////////////////////////////////////////////

}
