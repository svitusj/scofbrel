// Array lists required
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

public class ObjectStatStore
{
  /////////////////////////////////////////////////////////////////////////////
  
  private ArrayList< ObjectStat > mStats; ///< Stat list

  /////////////////////////////////////////////////////////////////////////////

  public ObjectStatStore()
  {
    mStats = new ArrayList< ObjectStat >();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public int GetNumberOfStats()
  {
    return mStats.size();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  ArrayList< ObjectStat > GetStats()
  {
    return mStats;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void AddStat( ObjectStat objectStat )
  {
    mStats.add( objectStat );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void CalculateAndCreateCSVFile( String statLabel, String codeFragmentTypeLabel, boolean isRankTagUsed, boolean isMethodFitTagUsed )
  {
    int numberOfMatches           = 0;
    int modelSize                 = 0;
    int featureSize               = 0;
    int numberOfTruePositivesAVG  = 0;
    int numberOfFalseNegativesAVG = 0;
    int numberOfFalsePositivesAVG = 0;
    int numberOfTrueNegativesAVG  = 0;
    
    for ( int i = 0; i < mStats.size(); ++i ) {
      if ( mStats.get( i ).mLabel.equals( statLabel ) ) {
        if ( numberOfMatches == 0 ) {
          modelSize   = mStats.get( i ).mModelSize;
          featureSize = mStats.get( i ).mFeatureSize;
        }
        ++numberOfMatches;
        numberOfTruePositivesAVG  += mStats.get( i ).mNumberOfTruePositives;
        numberOfFalseNegativesAVG += mStats.get( i ).mNumberOfFalseNegatives;
        numberOfFalsePositivesAVG += mStats.get( i ).mNumberOfFalsePositives;
        numberOfTrueNegativesAVG  += mStats.get( i ).mNumberOfTrueNegatives;
      }
    }
    
    if ( numberOfMatches > 0 ) {
      numberOfTruePositivesAVG  /= numberOfMatches;
      numberOfFalseNegativesAVG /= numberOfMatches;
      numberOfFalsePositivesAVG /= numberOfMatches;
      numberOfTrueNegativesAVG  /= numberOfMatches;
      
      PrintStream csvPrintStream = null;
      try {
        csvPrintStream =
          new PrintStream(
            new FileOutputStream( ( isRankTagUsed ? ( "RANK_" + statLabel ) : statLabel ) + ( isMethodFitTagUsed ? "MFIT_" : "" ) + "_" + codeFragmentTypeLabel + ".csv" )
          );
      }
      catch ( FileNotFoundException e ) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      System.setOut( csvPrintStream );

      //FS = TP + FN
      //MS = TP + FN + FP + TN
      boolean isInconsistencyFound =
          ( numberOfTruePositivesAVG + numberOfFalseNegativesAVG ) != featureSize ||
          ( numberOfTruePositivesAVG + numberOfFalseNegativesAVG + numberOfFalsePositivesAVG + numberOfTrueNegativesAVG ) != modelSize;
      
      System.out.println( ";MS;FS;TP;FN;FP;TN;" );
      System.out.println(
        ( isInconsistencyFound ? ( "//" + statLabel ) : statLabel ) + ";" + // Label
        modelSize + ";" + // Model size
        featureSize + ";" + // Requirement size
        numberOfTruePositivesAVG + ";" +
        numberOfFalseNegativesAVG + ";" +
        numberOfFalsePositivesAVG + ";" +
        numberOfTrueNegativesAVG + ";"
          );
      
      //FS = TP + FN
      //MS = TP + FN + FP + TN
      if ( isInconsistencyFound ) {
        if ( ( numberOfTruePositivesAVG + numberOfFalseNegativesAVG ) != featureSize ) {
          int TP_FN = numberOfTruePositivesAVG + numberOfFalseNegativesAVG;
          if ( TP_FN > featureSize ) {
            numberOfFalseNegativesAVG -= TP_FN - featureSize;
          }
          else {
            numberOfTruePositivesAVG += featureSize - TP_FN;
          }
        }
        if ( ( numberOfTruePositivesAVG + numberOfFalseNegativesAVG + numberOfFalsePositivesAVG + numberOfTrueNegativesAVG ) != modelSize ) {
          int TP_FN_FP_TN = numberOfTruePositivesAVG + numberOfFalseNegativesAVG + numberOfFalsePositivesAVG + numberOfTrueNegativesAVG;
          if ( TP_FN_FP_TN > modelSize ) {
            numberOfFalsePositivesAVG -= TP_FN_FP_TN - modelSize;
          }
          else {
            numberOfTrueNegativesAVG += modelSize - TP_FN_FP_TN;
          }
        }
        
        System.out.println(
            statLabel + ";" + // Label
            modelSize + ";" + // Model size
            featureSize + ";" + // Requirement size
            numberOfTruePositivesAVG + ";" +
            numberOfFalseNegativesAVG + ";" +
            numberOfFalsePositivesAVG + ";" +
            numberOfTrueNegativesAVG + ";"
              );
      }
      
      if ( csvPrintStream != null ) {
        csvPrintStream.close();
      }
    }

  }
  
  /////////////////////////////////////////////////////////////////////////////
}

