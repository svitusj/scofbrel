// Array lists required
import java.util.ArrayList;

public class ObjectFileStore
{
  /////////////////////////////////////////////////////////////////////////////
  
  private ObjectParser                mObjectParser;     ///< Parser
  
  private ArrayList< ObjectFileData > mOptionFiles;      ///< Options file   
  
  private ArrayList< ObjectFileData > mRequirementFiles; ///< Requirement file list
  
  private ArrayList< ObjectFileData > mSourceCodeFiles;  ///< Source code file list

  /////////////////////////////////////////////////////////////////////////////

  public ObjectFileStore()
  {
    mObjectParser = new ObjectParser();
    mOptionFiles = new ArrayList< ObjectFileData >();
    mRequirementFiles = new ArrayList< ObjectFileData >();
    mSourceCodeFiles = new ArrayList< ObjectFileData >();
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void SetOptionDirectoryName( String currentDirectoryName )
  {
    mObjectParser.SetOptionDirectoryName( currentDirectoryName );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void ParseOptionDirectory()
  {
    mOptionFiles = mObjectParser.ParseOptionDirectory();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  ArrayList< ObjectFileData > GetOptionFiles()
  {
    return mOptionFiles;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public int GetNumberOfSourceCodeFilesStored()
  {
    return mSourceCodeFiles.size();
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void SetSourceCodeDirectoryName( String currentDirectoryName )
  {
    mObjectParser.SetSourceCodeDirectoryName( currentDirectoryName );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void ParseSourceCodeDirectory()
  {
    mSourceCodeFiles = mObjectParser.ParseSourceCodeDirectory();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  ArrayList< ObjectFileData > GetSourceCodeFiles()
  {
    return mSourceCodeFiles;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void SetRequirementDirectoryName( String currentDirectoryName )
  {
    mObjectParser.SetRequirementDirectoryName( currentDirectoryName );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void ParseRequirementDirectory()
  {
    mRequirementFiles = mObjectParser.ParseRequirementDirectory();
    for ( int i = 0; i < mRequirementFiles.size(); ++i ) {
      if ( !mRequirementFiles.get( i ).GetLines().isEmpty() ) {
        mRequirementFiles.get( i ).SetOptionalDescription( mRequirementFiles.get( i ).GetLine( 0 ).third );
      }
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////

  ArrayList< ObjectFileData > GetRequirementFiles()
  {
    return mRequirementFiles;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public String GetCodeFragmentContent( String label, ObjectCodeFragment codeFragment, boolean isDebugTextAdded )
  {
    String fullText = "";
    if ( isDebugTextAdded ) {
      fullText +=
        ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE + 
        "Code Fragment: " + label + ObjectParser.CONSTANT_NEW_LINE_STRING +
        "Lines: " + codeFragment.size() + ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE;
    }
  
    int maximumLocalLineIndexInFragment = 0;
    int currentFragmentLocalLineIndex = 0;
    int maximumGlobalLineIndexInFragment = 0;
    int currentFragmentGlobalLineIndex = 0;
    for ( int i = 0; i < codeFragment.size(); ++i ) {
      currentFragmentLocalLineIndex = codeFragment.get( i ).second;
      if ( currentFragmentLocalLineIndex > maximumLocalLineIndexInFragment ) {
        maximumLocalLineIndexInFragment = currentFragmentLocalLineIndex;
      }
      currentFragmentGlobalLineIndex = codeFragment.get( i ).third;
      if ( currentFragmentGlobalLineIndex > maximumGlobalLineIndexInFragment ) {
        maximumGlobalLineIndexInFragment = currentFragmentGlobalLineIndex;
      }
    }
    String fileStringFormat = "%0" + ( ( mSourceCodeFiles.size() < 1 ) ? 1 : ( ( int )Math.log10( mSourceCodeFiles.size() ) + 1 ) ) + "d";
    String localLineStringFormat = "%0" + ( ( maximumLocalLineIndexInFragment < 1 ) ? 1 : ( ( int )Math.log10( maximumLocalLineIndexInFragment ) + 1 ) ) + "d";
    String globalLineStringFormat = "%0" + ( ( maximumGlobalLineIndexInFragment < 1 ) ? 1 : ( ( int )Math.log10( maximumGlobalLineIndexInFragment ) + 1 ) ) + "d";
    String line = "";
    boolean isEmptyLine = true;
    for ( int j = 0; j < codeFragment.size(); ++j ) {
      isEmptyLine = codeFragment.get( j ).first == ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID;
      if ( isDebugTextAdded ) {
        line = isEmptyLine ? " " : mSourceCodeFiles.get( codeFragment.get( j ).first ).GetLine( codeFragment.get( j ).second ).third;
        fullText +=
         "File " + String.format( fileStringFormat, codeFragment.get( j ).first ) + " : " +
         "Local Line " + String.format( localLineStringFormat, codeFragment.get( j ).second ) + " : " +
         "Global Line " + String.format( globalLineStringFormat, codeFragment.get( j ).third ) + " : "  + line + ObjectParser.CONSTANT_NEW_LINE_STRING;
      }
      else {
        fullText += isEmptyLine ? " " : mSourceCodeFiles.get( codeFragment.get( j ).first ).GetLine( codeFragment.get( j ).second ).third;
      }
    }
    return fullText;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public String GetCodeFragmentContent( ObjectCodeFragment codeFragment )
  {
    return GetCodeFragmentContent( "", codeFragment, false );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectWordSet GetCodeFragmentWords( ObjectCodeFragment codeFragment )
  {
    ObjectWordSet words = new ObjectWordSet();
    for ( int j = 0; j < codeFragment.size(); ++j ) {
      if ( codeFragment.get( j ).first != ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID ) {
        words.addAll( mSourceCodeFiles.get( codeFragment.get( j ).first ).GetLine( codeFragment.get( j ).second ).fourth );
      }
    }
    return words;
  }
  
  /////////////////////////////////////////////////////////////////////////////
}


