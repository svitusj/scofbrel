/**
* @author Daniel Blasco
*/

// List management required
import java.text.SimpleDateFormat;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ObjectSCoFBReL
{
  /////////////////////////////////////////////////////////////////////////////

  public enum ResultType
  {
    RESULT_TYPE_SEARCHING,
    RESULT_TYPE_PROPER_SOLUTION_FOUND,
    RESULT_TYPE_MAXIMUM_NUMBER_OF_STEPS_REACHED,
    RESULT_TYPE_MAXIMUM_NUMBER_OF_SECONDS_REACHED,
    RESULT_TYPES
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public static final String  CONSTANT_REQUIREMENT_DIRECTORY_DEFAULT  = "./Requirements/";
  
  public static final String  CONSTANT_SOURCE_CODE_DIRECTORY_DEFAULT  = "./TestCodeDirectory/";
  
  public static final boolean CONSTANT_IS_LOG_FILE_CREATED            = true;
  
  public static final boolean CONSTANT_IS_VERBOSE                     = true;
  
  public static final boolean CONSTANT_IS_FILE_PARSING_SHOWN          = false;
  
  public static final boolean CONSTANT_IS_CODE_FRAGMENT_CONTENT_SHOWN = true;
  
  public static final boolean CONSTANT_IS_CODE_FITNESS_PROCESS_SHOWN  = true;
  
  public static final boolean CONSTANT_IS_PARENT_FUSION_ALLOWED       = true;
  
  public static final boolean CONSTANT_IS_MUTATION_ALLOWED            = true;
  
  public static final boolean CONSTANT_IS_MUTANT_SPAWNED_FROM_CHILD   = true && CONSTANT_IS_MUTATION_ALLOWED;
  
  public static final boolean CONSTANT_IS_MUTATION_DRIVEN             = true && CONSTANT_IS_MUTATION_ALLOWED;
  
  public static final boolean CONSTANT_IS_SOURCE_CODE_EXPORTED        = false;
  
  public static final boolean CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION    = false;
  
  public static final boolean CONSTANT_IS_METHOD_GRANULARITY_USED     = false && !CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION;
  
  public static final boolean CONSTANT_IS_CODE_FRAGMENT_FIT_TO_METHOD = false && !CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION;
  
  public static final boolean CONSTANT_IS_BASELINE_GROUP_STAT_ALLOWED = true && CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION;
  
  public static final int     CONSTANT_BASELINE_GROUP_STAT_SIZE       = 10;
  
  public static final float   CONSTANT_BASELINE_GROUP_STAT_THRESHOLD  = 0.85f;
  
  public static final int     CONSTANT_NUMBER_OF_RUNS                 = CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION ? 1 : 30;
  
  public static final int     CONSTANT_MAXIMUM_NUMBER_OF_STEPS        = CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION ? 0 : -1;
  
  public static final int     CONSTANT_MINIMUM_NUMBER_OF_STEPS        = CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION ? 0 : 2;
  
  public static final long    CONSTANT_TIME_LIMIT_HOURS               = 0;
  
  public static final long    CONSTANT_TIME_LIMIT_MINUTES             = 20;
  
  public static final long    CONSTANT_TIME_LIMIT_SECONDS             = 0;
  
  public static final long    CONSTANT_MAXIMUM_NUMBER_OF_SECONDS      = CONSTANT_TIME_LIMIT_HOURS * 3600 + CONSTANT_TIME_LIMIT_MINUTES * 60 + CONSTANT_TIME_LIMIT_SECONDS;
  
  /////////////////////////////////////////////////////////////////////////////
  
  private long                   mStartTimeNanoseconds;          ///< Start time in nanoseconds
  
  private PrintStream            mLogPrintStream;                ///< Log print stream
  
  private ObjectFileStore        mObjectFileStore = null;        ///< File store
  
  private ObjectRequirementStore mObjectRequirementStore = null; ///< Requirement store
  
  private ObjectPopulationStore  mObjectPopulationStore = null;  ///< Population store
  
  private ObjectFitnessStore     mObjectFitnessStore = null;     ///< Fitness store
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectSCoFBReL()
  {
    this( "" );
  }

  /////////////////////////////////////////////////////////////////////////////

  public ObjectSCoFBReL( String label )
  {
    mStartTimeNanoseconds = System.nanoTime();
    
    if ( CONSTANT_IS_LOG_FILE_CREATED ) {
      
      String fileName =
          label.isEmpty() ?
          ( "Log_" + new SimpleDateFormat("[dd-MM-yyyy]").format( new Date() ) + "_" + new SimpleDateFormat("[HH�mm�ss]").format( Calendar.getInstance().getTime() ) + ".txt" ) :
          ( label + ".txt" );
      
      try {
        mLogPrintStream = new PrintStream( new FileOutputStream( fileName ) );
      }
      catch ( FileNotFoundException e ) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      System.setOut( mLogPrintStream );
    }
    else {
      mLogPrintStream = null;
    }
    
    mObjectFileStore = new ObjectFileStore();
    mObjectRequirementStore = new ObjectRequirementStore();
    mObjectPopulationStore = new ObjectPopulationStore();
    mObjectFitnessStore = new ObjectFitnessStore();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public static String GetCodeFragmentOrFunctionLabel()
  {
    return CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION ? "FUNC" : "FRAG";
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void SetFileStore( ObjectFileStore objectFileStore )
  {
    mObjectFileStore = objectFileStore;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void SetPopulationStore( ObjectPopulationStore objectPopulationStore )
  {
    mObjectPopulationStore = objectPopulationStore;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void SetIsCodeFragmentAFunction( boolean isCodeFragmentAFunction )
  {
    mObjectPopulationStore.SetIsCodeFragmentAFunction( isCodeFragmentAFunction );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public int SetQueryUpdateEnvironmentAndGetQueryIndex( String queryText )
  {
    // New queries
    int queryIndex = mObjectFitnessStore.AddQueryAndReturnIndex( new ObjectQueryData( queryText ) );
    
    // Query word list review
    if ( CONSTANT_IS_VERBOSE ) {
      mObjectFitnessStore.ListQueryWords();
    }
    
    // Queries processing
    mObjectFitnessStore.UpdateWordCountInQueries();
    
    return queryIndex;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void InitializeRequirements()
  {
    // Requirement files parsing
    mObjectFileStore.SetRequirementDirectoryName( CONSTANT_REQUIREMENT_DIRECTORY_DEFAULT );
    mObjectFileStore.ParseRequirementDirectory();
    // Requirement creation
    mObjectRequirementStore.AddRequirementsFromFiles( mObjectFileStore.GetRequirementFiles() );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectRequirementData GetRequirementByLabel( String requirementLabel )
  {
    return mObjectRequirementStore.GetRequirementByLabel( requirementLabel );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< Integer > InitializePopulationAndGetCodeFragmentIndices( String requirementLabel )
  {
    // Source code Parsing
    mObjectFileStore.SetSourceCodeDirectoryName( CONSTANT_SOURCE_CODE_DIRECTORY_DEFAULT );
    mObjectFileStore.ParseSourceCodeDirectory();
    // File lines storage for future population creation
    mObjectPopulationStore.AddContent( mObjectFileStore.GetSourceCodeFiles() );
    
    
    
    // Complete source code exported
    if ( CONSTANT_IS_SOURCE_CODE_EXPORTED ) {
      ExportSourceCode( "SourceCode.txt" );
    }
    
    // Original code fragment creation and storage
    ArrayList< Integer > codeFragmentIndices = new ArrayList< Integer >();
    
    
    int numberOfCodeFragmentsNeededLeft = ObjectPopulationStore.CONSTANT_INITIAL_POPULATION_SIZE;
    
    if ( !mObjectPopulationStore.IsCodeFragmentAFunction() ) {
      // Hot zones first fragments
      Random random = new Random( System.nanoTime() );
      boolean isKeywordSearchPending = true;
      ObjectWordSet objectWordSet = new ObjectWordSet( mObjectRequirementStore.GetRequirementByLabel( requirementLabel ).GetRequirementDescription() );
      if ( objectWordSet.size() > 1 ) {
        String primaryWord = objectWordSet.get( 0 );
        String secondaryWord = objectWordSet.get( 1 );
        if ( isKeywordSearchPending ) {
          System.out.println( "Primary Keyword: " + primaryWord );
          System.out.println( "Secondary Keyword: " + secondaryWord );
          isKeywordSearchPending = false;
        }

        ArrayList< ObjectFileData > sourceFiles =  mObjectFileStore.GetSourceCodeFiles();
        ObjectCodeFragment codeFragmentCompleteSourceCode = mObjectPopulationStore.GetCodeFragmentCompleteSourceCode();
        for ( int f = 0; numberOfCodeFragmentsNeededLeft > 0 && f < codeFragmentCompleteSourceCode.size(); ++f ) {
          if ( codeFragmentCompleteSourceCode.get( f ).first != ObjectCodeFragmentElement.CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID ) {
            ObjectWordSet wordsInLine = ( sourceFiles.get( codeFragmentCompleteSourceCode.get( f ).first ).GetLine( codeFragmentCompleteSourceCode.get( f ).second ).fourth );
            if ( wordsInLine.contains( primaryWord ) && wordsInLine.contains( secondaryWord ) ) {

              int globalLineIndex1 = codeFragmentCompleteSourceCode.get( f ).third;
              ObjectCodeFragment codeFragment = mObjectPopulationStore.CreateCodeFragment(
                  CONSTANT_IS_METHOD_GRANULARITY_USED ? -1 : ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_DEFAULT_SIZE,
                  ( random.nextInt( 2 ) == 0 ? 1 : -1 ) * random.nextInt( Math.max( 1, ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_DEFAULT_SIZE / 4 ) ) + globalLineIndex1 - ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_DEFAULT_SIZE / 2 );
              if ( !codeFragment.isEmpty() ) {
                int codeFragmentIndex = mObjectPopulationStore.AddCodeFragmentAndReturnIndex( codeFragment );
                codeFragmentIndices.add( codeFragmentIndex );
                --numberOfCodeFragmentsNeededLeft;
              }
            }
            if ( ( wordsInLine.contains( primaryWord ) && wordsInLine.contains( secondaryWord ) ) || wordsInLine.contains( primaryWord ) /*|| wordsInLine.contains( secondaryWord )*/ ) {
              int globalLineIndex2 = Math.max( 0, codeFragmentCompleteSourceCode.get( f ).third - ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_DEFAULT_SIZE / 2 );
              mObjectPopulationStore.AddSimplifiedCodeLineMutationIndex( globalLineIndex2 );
            }
          }
        }
      }
        
    }

    
    
    
    // Pseudo random code fragments left
    int pseudoRandomStep =
        ( mObjectPopulationStore.GetNumberOfLinesStored() / ( 1 + numberOfCodeFragmentsNeededLeft ) );

    for ( int i = 0;
          mObjectPopulationStore.IsCodeFragmentAFunction() ? ( mObjectPopulationStore.IsContiguousCodeFragmentGenerationStillPossible() ) : ( i < numberOfCodeFragmentsNeededLeft );
          ++i ) {
      // Note: for code fragment granularity, default size is overridden
      ObjectCodeFragment codeFragment = mObjectPopulationStore.CreateCodeFragment(
          CONSTANT_IS_METHOD_GRANULARITY_USED ? -1 : ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_DEFAULT_SIZE,
          ( i + 1 ) * pseudoRandomStep - ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_DEFAULT_SIZE / 2 );
      if ( !codeFragment.isEmpty() ) {
        int codeFragmentIndex = mObjectPopulationStore.AddCodeFragmentAndReturnIndex( codeFragment );
        codeFragmentIndices.add( codeFragmentIndex );
        if ( CONSTANT_IS_VERBOSE && CONSTANT_IS_CODE_FRAGMENT_CONTENT_SHOWN ) {
          // Original code fragments content
          System.out.println( mObjectFileStore.GetCodeFragmentContent( String.valueOf( codeFragmentIndex ), codeFragment, CONSTANT_IS_VERBOSE ) );
        }
      }
      else {
        --i;
      }
    }
    
    return codeFragmentIndices;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< Integer > ApplyGeneticOperationsAndGetCodeFragmentIndices( ArrayList< Integer > eventuallyMutatedCodeFragmentIndices, ArrayList< Integer > futureParentsCodeFragmentIndices )
  {
    // Mutation
    if ( CONSTANT_IS_MUTATION_ALLOWED && !CONSTANT_IS_MUTANT_SPAWNED_FROM_CHILD ) {
      for ( int i = 0; i < futureParentsCodeFragmentIndices.size(); ++i ) {
        if( mObjectPopulationStore.MutateAndGetSuccess( futureParentsCodeFragmentIndices.get( i ), ObjectPopulationStore.CONSTANT_MUTATION_PROBABILITY_DEFAULT ) &&
            CONSTANT_IS_VERBOSE) {
            System.out.println( "Mutation by Code Fragment: " + futureParentsCodeFragmentIndices.get( i ) );
        }
      }
    }
    
    // Original code fragments produce new ones through crossover
    ArrayList< Integer > newCodeFragmentIndices = new ArrayList< Integer >();
    ObjectPair< Integer, Integer > newCodeFragmentIndexPair = new ObjectPair< Integer, Integer >( 0, 0 );
    
    if ( CONSTANT_IS_PARENT_FUSION_ALLOWED ) {
      int newCodeFragmentIndexFromParentFusion = mObjectPopulationStore.FuseAndGetNewIndex( futureParentsCodeFragmentIndices );
      if ( newCodeFragmentIndexFromParentFusion != ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_INDEX_INVALID ) {
        newCodeFragmentIndices.add( newCodeFragmentIndexFromParentFusion );
        System.out.println( futureParentsCodeFragmentIndices.toString() );
        System.out.println( mObjectFileStore.GetCodeFragmentContent(
            String.valueOf( newCodeFragmentIndexFromParentFusion ),
            mObjectPopulationStore.GetCodeFragment( newCodeFragmentIndexFromParentFusion ), CONSTANT_IS_VERBOSE ) );
      }
    }
    
    for ( int j = 0; j < futureParentsCodeFragmentIndices.size(); ++j ) {
      for ( int k = j + 1; k < futureParentsCodeFragmentIndices.size(); ++k ) {
        newCodeFragmentIndexPair = mObjectPopulationStore.CrossoverAddAndGetNewIndices( j, k );
        if ( newCodeFragmentIndexPair.first != ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_INDEX_INVALID ) {
          newCodeFragmentIndices.add( newCodeFragmentIndexPair.first );
        }
        if ( newCodeFragmentIndexPair.second != ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_INDEX_INVALID ) {
          newCodeFragmentIndices.add( newCodeFragmentIndexPair.second );
        }
        
        if ( CONSTANT_IS_VERBOSE && CONSTANT_IS_CODE_FRAGMENT_CONTENT_SHOWN ) {
          // New code fragments content
          if ( newCodeFragmentIndexPair.first != ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_INDEX_INVALID ) {
            System.out.println( mObjectFileStore.GetCodeFragmentContent(
              String.valueOf( newCodeFragmentIndexPair.first ),
              mObjectPopulationStore.GetCodeFragment( newCodeFragmentIndexPair.first ), CONSTANT_IS_VERBOSE ) );
          }
          if ( newCodeFragmentIndexPair.second != ObjectCodeFragment.CONSTANT_CODE_FRAGMENT_INDEX_INVALID ) {
            System.out.println( mObjectFileStore.GetCodeFragmentContent(
                String.valueOf( newCodeFragmentIndexPair.second ),
                mObjectPopulationStore.GetCodeFragment( newCodeFragmentIndexPair.second ), CONSTANT_IS_VERBOSE ) );
          }
        }
      }
    }
    
    if ( CONSTANT_IS_VERBOSE ) {
      System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + "Number of new code fragments: " + newCodeFragmentIndices.size() );
      for ( int i = 0; i < newCodeFragmentIndices.size(); ++i ) {
        System.out.println( "Code Fragment: " + newCodeFragmentIndices.get( i ) );
      }
    }
    
    // Mutants are copies of children
    if ( CONSTANT_IS_MUTATION_ALLOWED && CONSTANT_IS_MUTANT_SPAWNED_FROM_CHILD ) {
      int originalNewCodeFragmentIndicesSize = newCodeFragmentIndices.size();
      for ( int i = 0; i < originalNewCodeFragmentIndicesSize; ++i ) {
        ObjectCodeFragment spawnedCodeFragment = new ObjectCodeFragment();
        spawnedCodeFragment.SetContent( mObjectPopulationStore.GetCodeFragment( newCodeFragmentIndices.get( i ) ) );
        int spawnedCodeFragmentIndex = mObjectPopulationStore.AddCodeFragmentAndReturnIndex( spawnedCodeFragment );
        if( mObjectPopulationStore.MutateAndGetSuccess( spawnedCodeFragmentIndex, ObjectPopulationStore.CONSTANT_MUTATION_PROBABILITY_DEFAULT ) &&
            CONSTANT_IS_VERBOSE) {
          newCodeFragmentIndices.add( spawnedCodeFragmentIndex );
          System.out.println( "Spawned Mutated Code Fragment: " + spawnedCodeFragmentIndex );

          if ( CONSTANT_IS_CODE_FRAGMENT_CONTENT_SHOWN ) {
            System.out.println( mObjectFileStore.GetCodeFragmentContent(
                String.valueOf( spawnedCodeFragmentIndex ),
                mObjectPopulationStore.GetCodeFragment( spawnedCodeFragmentIndex ),
                CONSTANT_IS_VERBOSE ) );
          }
        }
        else {
          mObjectPopulationStore.RemoveCodeFragment( spawnedCodeFragmentIndex );
        }
      }
    }
    
    return newCodeFragmentIndices;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< ObjectPair< Integer, Double > > CalculateFitness( int queryIndex, ArrayList< Integer > codeFragmentIndices )
  {
    ArrayList< ObjectPair< Integer, Double > > codeFragmentInddexfitnessValues = new ArrayList< ObjectPair< Integer, Double > >();
    
    if ( ObjectFitnessStore.CONSTANT_IS_EXTERNAL_FITNESS_USED ) {
      ArrayList<ObjectWordSet> codeFragmentWordSets = new ArrayList<ObjectWordSet>(); 
      for ( int g = 0; g < codeFragmentIndices.size(); ++g ) {
        codeFragmentWordSets.add( mObjectFileStore.GetCodeFragmentWords( mObjectPopulationStore.GetCodeFragment( codeFragmentIndices.get( g ) ) ) );
      }
      ArrayList<Double> fitnessValues = mObjectFitnessStore.CalculateFitness( queryIndex, codeFragmentIndices, codeFragmentWordSets );
      for ( int h = 0; h < fitnessValues.size(); ++h ) {
        codeFragmentInddexfitnessValues.add( new ObjectPair<Integer, Double>( codeFragmentIndices.get( h ), fitnessValues.get( h ) ) );
      }
    }
    else {
      for ( int i = 0; i < codeFragmentIndices.size(); ++i ) {
        codeFragmentInddexfitnessValues.add
        ( 
            new ObjectPair< Integer, Double> (
              codeFragmentIndices.get( i ),
              mObjectFitnessStore.CalculateFitness(
                queryIndex,
                codeFragmentIndices.get( i ),
                mObjectFileStore.GetCodeFragmentWords( mObjectPopulationStore.GetCodeFragment( codeFragmentIndices.get( i ) ) ) )
            )
         );
      }
    }
    
    return codeFragmentInddexfitnessValues;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void ControlPopulation( ArrayList< Integer > codeFragmentIndicesOriginal, ArrayList< Integer > codeFragmentIndicesToRemove )
  {
    if ( CONSTANT_IS_VERBOSE ) {
      System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + "Number of code fragments to be discarded: " + codeFragmentIndicesToRemove.size() + " of " + codeFragmentIndicesOriginal.size() );
      for ( int i = 0; i < codeFragmentIndicesToRemove.size(); ++i ) {
        System.out.println( "Code Fragment: " + codeFragmentIndicesToRemove.get( i ) );
      }
    }
    codeFragmentIndicesOriginal.removeAll( codeFragmentIndicesToRemove );
    if ( CONSTANT_IS_VERBOSE ) {
      System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + "Population size: " + codeFragmentIndicesOriginal.size() );
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ResultType GetCurrentResult( int currentStepIndex, ArrayList< ObjectPair< Integer, Double > > codeFragmentIndexFitnessValues )
  {
    ResultType resultType = ResultType.RESULT_TYPE_SEARCHING;
    
    boolean isSearchRequired = true;
    for ( int i = 0; isSearchRequired && i < codeFragmentIndexFitnessValues.size(); ++i ) {
      if ( codeFragmentIndexFitnessValues.get( i ).second >= ObjectFitnessStore.CONSTANT_FITNESS_TARGET_THRESHOLD ) {
        isSearchRequired = false;
      }
    }
    
    if ( isSearchRequired || currentStepIndex < CONSTANT_MINIMUM_NUMBER_OF_STEPS ) {
      if ( CONSTANT_MAXIMUM_NUMBER_OF_STEPS < 0 ) {
        if ( TimeUnit.NANOSECONDS.toSeconds( System.nanoTime() - mStartTimeNanoseconds ) >= CONSTANT_MAXIMUM_NUMBER_OF_SECONDS ) {
          resultType = ResultType.RESULT_TYPE_MAXIMUM_NUMBER_OF_SECONDS_REACHED;
        }
      }
      else if ( currentStepIndex >= CONSTANT_MAXIMUM_NUMBER_OF_STEPS ) {
        resultType = ResultType.RESULT_TYPE_MAXIMUM_NUMBER_OF_STEPS_REACHED;
      }
    }
    else {
      resultType = ResultType.RESULT_TYPE_PROPER_SOLUTION_FOUND;
    }
    
    return resultType;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public int ReviewResultAndGetSolutionCodeFragmentIndex( ResultType finalResult, int numberOfStepsDone, ArrayList< ObjectPair< Integer, Double > > codeFragmentIndexFitnessValues )
  {
    int solutionCodeFragmentIndex = -1;
    
    if ( CONSTANT_IS_VERBOSE ) {
      long timeDifferenceNanoseconds = System.nanoTime() - mStartTimeNanoseconds;
      System.out.println(
        ObjectParser.CONSTANT_NEW_LINE_STRING +
        "PROCESS FINISHED. TIME ELAPSED: " +
            String.format("%02d:%02d:%02d", 
              TimeUnit.NANOSECONDS.toHours( timeDifferenceNanoseconds ),
              TimeUnit.NANOSECONDS.toMinutes( timeDifferenceNanoseconds ) -  
              TimeUnit.HOURS.toMinutes( TimeUnit.NANOSECONDS.toHours( timeDifferenceNanoseconds ) ), // The change is in this line
              TimeUnit.NANOSECONDS.toSeconds( timeDifferenceNanoseconds ) - 
              TimeUnit.MINUTES.toSeconds( TimeUnit.NANOSECONDS.toMinutes( timeDifferenceNanoseconds ) ) ) );
    }
      
    switch( finalResult ) {
      case RESULT_TYPE_PROPER_SOLUTION_FOUND:
        {
          double solutionFitnessValue = 0.0f;
          for ( int i = 0; solutionCodeFragmentIndex < 0 && i < codeFragmentIndexFitnessValues.size(); ++i ) {
            if ( codeFragmentIndexFitnessValues.get( i ).second >= ObjectFitnessStore.CONSTANT_FITNESS_TARGET_THRESHOLD ) {
              solutionCodeFragmentIndex = codeFragmentIndexFitnessValues.get( i ).first;
              solutionFitnessValue = codeFragmentIndexFitnessValues.get( i ).second;
            }
          }
          if ( CONSTANT_IS_VERBOSE ) {
            System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + "Solution found: Code fragment: " + solutionCodeFragmentIndex + " Fitness value: " + String.valueOf( solutionFitnessValue ) );
            System.out.println( mObjectFileStore.GetCodeFragmentContent( String.valueOf( solutionCodeFragmentIndex ), mObjectPopulationStore.GetCodeFragment( solutionCodeFragmentIndex ), CONSTANT_IS_VERBOSE ) );
          }
        }
        break;
      
      case RESULT_TYPE_MAXIMUM_NUMBER_OF_STEPS_REACHED:
      case RESULT_TYPE_MAXIMUM_NUMBER_OF_SECONDS_REACHED:
       {
         int bestIndex = 0;
         double bestFitnessValue = 0.0f;
         for ( int i = 0; i < codeFragmentIndexFitnessValues.size(); ++i ) {
           if ( i == 0 || codeFragmentIndexFitnessValues.get( i ).second > codeFragmentIndexFitnessValues.get( bestIndex ).second ) {
             bestIndex = i;
             bestFitnessValue = codeFragmentIndexFitnessValues.get( i ).second;
           }
         }
         solutionCodeFragmentIndex = codeFragmentIndexFitnessValues.get( bestIndex ).first;
         if ( CONSTANT_IS_VERBOSE ) {
           System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + ( finalResult == ResultType.RESULT_TYPE_MAXIMUM_NUMBER_OF_STEPS_REACHED ? "MAXIMUM NUMBER OF STEPS REACHED" : "TIME LIMIT REACHED" ) + ". Best code fragment: " + solutionCodeFragmentIndex + " Fitness value: " + String.valueOf( bestFitnessValue )  );
           System.out.println( mObjectFileStore.GetCodeFragmentContent( String.valueOf( solutionCodeFragmentIndex ), mObjectPopulationStore.GetCodeFragment( solutionCodeFragmentIndex ), CONSTANT_IS_VERBOSE ) );
         }  
       }
       break;
        
      default:
        if ( CONSTANT_IS_VERBOSE ) {
          System.out.println( "Result type out of range." );
        }
        break;
    }
    
    return solutionCodeFragmentIndex;
  }

  /////////////////////////////////////////////////////////////////////////////
  
  public void ExportSourceCode( String fileName )
  {
    String completeSourceCodeContent =
      mObjectFileStore.GetCodeFragmentContent(
        "Complete Source Code." + ObjectParser.CONSTANT_NEW_LINE_STRING +
        "Number of files: " + mObjectFileStore.GetNumberOfSourceCodeFilesStored(),
        mObjectPopulationStore.GetCodeFragmentCompleteSourceCode(),
        true );
    
    FileWriter fileWriter;
    try {
      fileWriter = new FileWriter( fileName );
      fileWriter.write( completeSourceCodeContent );
      fileWriter.close();
    }
    catch ( IOException e1 ) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectStat CreateStat( ObjectRequirementData objectRequirementData, ArrayList< ObjectCodeFragment > objectsCodeFragmentData )
  {
    ArrayList<Integer> requirementLineIndices = objectRequirementData.GetLineIndices();
    
     
    int numberOfTruePositives = 0;
    int numberOfFalsePositives = 0;
    int numberOfTrueNegatives = 0;
    int numberOfFalseNegatives = 0;
   
    
    boolean isMissing = true;
    for ( int i = 0; i < requirementLineIndices.size(); ++i ) {
      isMissing = true;
      int indexInRequirement = requirementLineIndices.get( i );
      for ( int j = 0; isMissing && j < objectsCodeFragmentData.size(); ++j ) {
        for ( int k = 0; isMissing && k < objectsCodeFragmentData.get( j ).size(); ++k ) {
          if ( indexInRequirement == objectsCodeFragmentData.get( j ).get( k ).third ) {
            ++numberOfTruePositives;
            isMissing = false;
          }
        }
      }
      if ( isMissing ) {
        ++numberOfFalseNegatives;
      }
    }
    
    
    ArrayList< Boolean > isLineInCodeFragment = new ArrayList< Boolean >();
    for ( int h = 0; h < mObjectPopulationStore.GetNumberOfLinesStored(); ++h ) {
      isLineInCodeFragment.add( false );
    }
    
    
    ArrayList< Integer > codeFragmentIndicesReviewed = new ArrayList< Integer >();
    isMissing = true;
    for ( int j = 0; j < objectsCodeFragmentData.size(); ++j ) {
      for ( int k = 0; k < objectsCodeFragmentData.get( j ).size(); ++k ) {
        isMissing = true;
        int indexInCodeFragment = objectsCodeFragmentData.get( j ).get( k ).third;
        if ( !codeFragmentIndicesReviewed.contains( indexInCodeFragment ) ) {
          codeFragmentIndicesReviewed.add( indexInCodeFragment );
          isLineInCodeFragment.set( indexInCodeFragment, true );
          for ( int m = 0; isMissing && m < requirementLineIndices.size(); ++m ) {
            if ( indexInCodeFragment == requirementLineIndices.get( m ) ) {
              isMissing = false;
            }
          }
          if ( isMissing ) {
            ++numberOfFalsePositives;
          }
        }
      }
    }
    
    
    isMissing = true;
    for ( int n = 0; n < isLineInCodeFragment.size(); ++n ) {
      if ( !isLineInCodeFragment.get( n ) ) {
        isMissing = true;
        int negativeIndexInCodeFragment = n;
        for ( int p = 0; isMissing && p < requirementLineIndices.size(); ++p ) {
          if ( negativeIndexInCodeFragment == requirementLineIndices.get( p ) ) {
            isMissing = false;
          }
        }
        if ( isMissing ) {
          ++numberOfTrueNegatives;
        }
      }
    }
    

    return new ObjectStat(
        objectRequirementData.GetRequirementLabel(),
        mObjectPopulationStore.GetNumberOfLinesStored(),
        objectRequirementData.GetLineIndices().size(),
        numberOfTruePositives,
        numberOfFalseNegatives,
        numberOfFalsePositives,
        numberOfTrueNegatives );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ArrayList< Integer > GetBestCodeFragmentIndices( int numberOfIndices, float thresholdOverridingNumberOfIndices, ArrayList< ObjectPair< Integer, Double > > codeFragmentIndexFitnessValues )
  {
    final int finalNumberOfIndices = numberOfIndices >= 0 ? numberOfIndices : (int)( (float)( codeFragmentIndexFitnessValues.size() ) * (float)(-numberOfIndices) / 100.0f );
    ArrayList< Integer > bestCodeFragmentIndices = new ArrayList< Integer >();
    
    for ( int i = 0; i < codeFragmentIndexFitnessValues.size(); ++i ) {
      double fitnessValue = codeFragmentIndexFitnessValues.get( i ).second;
      if ( bestCodeFragmentIndices.size() < finalNumberOfIndices || fitnessValue >= thresholdOverridingNumberOfIndices ) {
        bestCodeFragmentIndices.add( i );
      }
      else {
        boolean isTryingToAdd = true;
        for ( int j = 0; isTryingToAdd && j < bestCodeFragmentIndices.size(); ++j ) {
          if ( fitnessValue > codeFragmentIndexFitnessValues.get( bestCodeFragmentIndices.get( j ) ).second ) {
            bestCodeFragmentIndices.set( j, i );
            isTryingToAdd = false;
          }
        }
      }
    }
    
    System.out.println( "hey: " + bestCodeFragmentIndices.toString() );
    
    for ( int k = 0; k < bestCodeFragmentIndices.size(); ++k ) {
      int codeFragmentIndex = codeFragmentIndexFitnessValues.get( bestCodeFragmentIndices.get( k ) ).first;
      double fitValue = codeFragmentIndexFitnessValues.get( bestCodeFragmentIndices.get( k ) ).second;
      bestCodeFragmentIndices.set( k, codeFragmentIndex );
      if ( CONSTANT_IS_VERBOSE ) {
        System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + "Ranking: Position #" + ( k + 1 ) + ": Code fragment: " + codeFragmentIndex + " Fitness value: " + String.valueOf( fitValue ) );
        System.out.println( mObjectFileStore.GetCodeFragmentContent( String.valueOf( codeFragmentIndex ), mObjectPopulationStore.GetCodeFragment( codeFragmentIndex ), CONSTANT_IS_VERBOSE ) );
      }
    }
    
    return bestCodeFragmentIndices;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void Finish()
  {
    if ( mLogPrintStream != null ) {
      mLogPrintStream.close();
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectStat Run( String requirementLabel )
  {
    // General Variables
    ArrayList< Integer > initialCodeFragmentIndices = new ArrayList< Integer >();
    ArrayList< Integer > selectedCodeFragmentIndices = new ArrayList< Integer >();
    ArrayList< Integer > finalCodeFragmentIndices = new ArrayList< Integer >();
    ArrayList< ObjectPair< Integer, Double > > codeFragmentIndexFitnessValues = new ArrayList< ObjectPair< Integer, Double > >();
    int stepIndex = -1;
    ResultType currentResult = ResultType.RESULT_TYPE_SEARCHING;
    
    // New query
    int queryIndex = SetQueryUpdateEnvironmentAndGetQueryIndex( mObjectRequirementStore.GetRequirementByLabel( requirementLabel ).GetRequirementDescription() );
    
    
   
    do
    {
      // New step start
      ++stepIndex;
      
      if ( CONSTANT_IS_VERBOSE ) {
        System.out.print( ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE + "STEP: " + stepIndex + ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE );
      }
      
      // Population initialization
      initialCodeFragmentIndices.clear();
      initialCodeFragmentIndices.addAll(
          ( stepIndex > 0 ) ?
          finalCodeFragmentIndices :
          InitializePopulationAndGetCodeFragmentIndices( requirementLabel ) );
      
      if ( stepIndex > 0 ) {
        // Fitness evaluation
        codeFragmentIndexFitnessValues.clear();
        codeFragmentIndexFitnessValues.addAll( CalculateFitness( queryIndex, initialCodeFragmentIndices ) );
        
        // Current result
        currentResult = GetCurrentResult( stepIndex, codeFragmentIndexFitnessValues );
        
        if ( currentResult == ResultType.RESULT_TYPE_SEARCHING ) {
        
          // Selection
          selectedCodeFragmentIndices.clear();
          //selectedCodeFragmentIndices.addAll( initialCodeFragmentIndices );
          //System.out.println( "Fittest from: " + codeFragmentIndexFitnessValues.size() );
          selectedCodeFragmentIndices.addAll( mObjectPopulationStore.GetFittest(
              codeFragmentIndexFitnessValues,
              ObjectCodeFragment.CONSTANT_IS_NEO_CROSSOVER_ENABLED ? 7 : 2/*Math.max( 2, codeFragmentIndexFitnessValues.size() / 2 )*/ ) );
          
          // Genetic operations
          finalCodeFragmentIndices.clear();
          finalCodeFragmentIndices.addAll( ApplyGeneticOperationsAndGetCodeFragmentIndices( initialCodeFragmentIndices, selectedCodeFragmentIndices ) );
          
          // Final population
          finalCodeFragmentIndices.addAll( initialCodeFragmentIndices );
          
          // Fitness evaluation
          codeFragmentIndexFitnessValues.clear();
          codeFragmentIndexFitnessValues.addAll( CalculateFitness( queryIndex, finalCodeFragmentIndices ) );
          
          // Population control
          if ( codeFragmentIndexFitnessValues.size() > ObjectPopulationStore.CONSTANT_INITIAL_POPULATION_SIZE ) {
            ControlPopulation( finalCodeFragmentIndices, mObjectPopulationStore.GetWorst( codeFragmentIndexFitnessValues,  codeFragmentIndexFitnessValues.size() - ObjectPopulationStore.CONSTANT_INITIAL_POPULATION_SIZE  ) );
          }
        
        }
      }
      else {
        // Final population
        finalCodeFragmentIndices.clear();
        finalCodeFragmentIndices.addAll( initialCodeFragmentIndices );
        
        // Initial fitness evaluation
        codeFragmentIndexFitnessValues.clear();
        codeFragmentIndexFitnessValues.addAll( CalculateFitness( queryIndex, finalCodeFragmentIndices ) );
      }
      
      // Current result
      currentResult = GetCurrentResult( stepIndex, codeFragmentIndexFitnessValues );
    }
    while ( currentResult == ResultType.RESULT_TYPE_SEARCHING );
    
    // Result review
    int solutionCodeFragmentIndex = ReviewResultAndGetSolutionCodeFragmentIndex( currentResult, stepIndex, codeFragmentIndexFitnessValues );
    
    // Process finished: Review and CSV file creation
    ArrayList< ObjectCodeFragment > codeFragments = new ArrayList< ObjectCodeFragment >();
    codeFragments.clear();
    ArrayList< Integer > bestCodeFragmentIndices;
    // Additional stats in some cases
    if ( mObjectPopulationStore.IsCodeFragmentAFunction() && CONSTANT_IS_BASELINE_GROUP_STAT_ALLOWED ) {
      System.out.println( "About to get best indices");
      bestCodeFragmentIndices = GetBestCodeFragmentIndices( CONSTANT_BASELINE_GROUP_STAT_SIZE, CONSTANT_BASELINE_GROUP_STAT_THRESHOLD, codeFragmentIndexFitnessValues );
      if ( bestCodeFragmentIndices.size() == 0 ) { System.out.println( "No best code fragments!!!"); }
      for ( int s = 0; s < bestCodeFragmentIndices.size(); ++s ) {
        System.out.println( "Best code fragment indices: " + bestCodeFragmentIndices.get( s ) );
        codeFragments.add(  mObjectPopulationStore.GetCodeFragment( bestCodeFragmentIndices.get( s ) ) );
      }
    }
    else {
      codeFragments.add( 
          CONSTANT_IS_CODE_FRAGMENT_FIT_TO_METHOD ?
          mObjectPopulationStore.SetAndGetCodeFragmentFitToMethodVersion( solutionCodeFragmentIndex ) :
          mObjectPopulationStore.GetCodeFragment( solutionCodeFragmentIndex ) );
    }
    
    // Finish
    Finish();
    
    return CreateStat(
        mObjectRequirementStore.GetRequirementByLabel( requirementLabel ),
        codeFragments );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public static void main( String[] args )
  {
    boolean           isValidRequirement      = false;
    ArrayList<String> requirementLabels       = new ArrayList<String>();
    
    requirementLabels.add( "R-1" );
    requirementLabels.add( "R-2" );
    requirementLabels.add( "R-3" );
    requirementLabels.add( "R-4" );
    requirementLabels.add( "R-5" );
    requirementLabels.add( "R-6" );
    requirementLabels.add( "R-7" );
    requirementLabels.add( "R-8" );
    requirementLabels.add( "R-9" );
    requirementLabels.add( "R-10" );
    requirementLabels.add( "R-11" );
    requirementLabels.add( "R-12" );
    requirementLabels.add( "R-13" );
    requirementLabels.add( "R-14" );
    requirementLabels.add( "R-15" );
    requirementLabels.add( "R-16" );
    requirementLabels.add( "R-17" );
    requirementLabels.add( "R-18" );
    requirementLabels.add( "R-19" );
    requirementLabels.add( "R-20" );
    requirementLabels.add( "R-21" );
    
    ObjectSCoFBReL        objectSCoFBReL    = null;
    
    for ( int i = 0; i < requirementLabels.size(); ++i ) {

      isValidRequirement = true;
      ObjectStatStore objectStatStore = new ObjectStatStore();

      for ( int j = 0; isValidRequirement && j < ObjectSCoFBReL.CONSTANT_NUMBER_OF_RUNS; ++j ) {
        System.gc();
  
        objectSCoFBReL = new ObjectSCoFBReL( requirementLabels.get( i ) + "_" + GetCodeFragmentOrFunctionLabel() + "_[" + ( j + 1 ) + "]" );
        objectSCoFBReL.SetIsCodeFragmentAFunction( CONSTANT_IS_CODE_FRAGMENT_A_FUNCTION );
        objectSCoFBReL.InitializeRequirements();
        if ( j == 0 ) {
          isValidRequirement = objectSCoFBReL.GetRequirementByLabel( requirementLabels.get( i ) ) != null;
        }
        if ( isValidRequirement ) {
          objectStatStore.AddStat( objectSCoFBReL.Run( requirementLabels.get( i ) ) );
        }
      }
      if ( isValidRequirement ) {
        objectStatStore.CalculateAndCreateCSVFile(
            requirementLabels.get( i ),
            GetCodeFragmentOrFunctionLabel(),
            CONSTANT_IS_BASELINE_GROUP_STAT_ALLOWED,
            CONSTANT_IS_CODE_FRAGMENT_FIT_TO_METHOD
             );
        
        
      }
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////

}
