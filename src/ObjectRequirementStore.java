// Array lists required
import java.util.ArrayList;

public class ObjectRequirementStore
{
  /////////////////////////////////////////////////////////////////////////////
  
  private ArrayList< ObjectRequirementData > mRequirements; ///< Requirement list

  /////////////////////////////////////////////////////////////////////////////

  public ObjectRequirementStore()
  {
    mRequirements = new ArrayList< ObjectRequirementData >();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public int GetNumberOfRequirements()
  {
    return mRequirements.size();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  ArrayList< ObjectRequirementData > GetRequirements()
  {
    return mRequirements;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectRequirementData GetRequirementByLabel( String requirementLabel )
  {
    ObjectRequirementData objectRequirementData= null;
    for ( int i = 0; i < mRequirements.size(); ++i ) {
      if ( mRequirements.get( i ).GetRequirementLabel().equals( requirementLabel ) ) {
        objectRequirementData = mRequirements.get( i );
      }
    }
    return objectRequirementData;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void AddRequirement( ObjectRequirementData objectRequirementData )
  {
    mRequirements.add( objectRequirementData );
  }
 
  /////////////////////////////////////////////////////////////////////////////
  
  public void AddRequirement(
      String requirementLabel,
      String requirementDescription,
      ArrayList< Integer > requirementLineIndices )
  {
    ObjectRequirementData objectRequirementData = new ObjectRequirementData();
    objectRequirementData.SetRequirementLabel( requirementLabel );
    objectRequirementData.SetRequirementDescription( requirementDescription );
    for ( int i = 0; i < requirementLineIndices.size(); ++i ) {
      objectRequirementData.AddLineIndex( requirementLineIndices.get( i ) );
    }
    mRequirements.add( objectRequirementData );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void AddRequirementsFromFiles( ArrayList< ObjectFileData > requirementFiles )
  {
    for ( int h = 0; h < requirementFiles.size(); ++h ) {
      ObjectFileData objectFileData = requirementFiles.get( h );
      ArrayList< ObjectCodeLine > lines = objectFileData.GetLines();
      
      // Comment removal
      for ( int i = lines.size() - 1; i >= 0; --i ) {
        if ( lines.get( i ).third.startsWith( "//" ) ) {
          lines.remove( i );
        }
      }
      
      if ( lines.size() > 3 ) {
        ObjectRequirementData objectRequirementData = new ObjectRequirementData();
        Boolean isEndReached = false;
        for ( int j = 0; j < lines.size() && !isEndReached; ++j ) {
          if ( j == 0 ) {
            objectRequirementData.SetRequirementLabel( lines.get( j ).third );
          }
          else if ( j == 1 ) {
            objectRequirementData.SetRequirementDescription( lines.get( j ).third );
          }
          else if ( lines.get( j ).third.contains( ";" ) ) {
            isEndReached = true;
          }
          else if ( lines.get( j ).third.contains( "-" ) ) {
            ObjectWordSet wordSet = ObjectUtils.GetTokenizedByCustomSeparator( lines.get( j ).third, "-" );
            if ( wordSet.size() == 2 ) {
              int lineIndexLimit = 1 + Integer.parseInt( wordSet.get( 1 ) );
              for ( int k = Integer.parseInt( wordSet.get( 0 ) ); k < lineIndexLimit; ++k ) {
                objectRequirementData.AddLineIndex( k );
              }
            }
          }
          else {
            objectRequirementData.AddLineIndex( Integer.parseInt( lines.get( j ).third ) );
          }
        }
        AddRequirement( objectRequirementData );
      }
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////
}


