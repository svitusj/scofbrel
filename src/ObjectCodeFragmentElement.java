public class ObjectCodeFragmentElement extends ObjectTrio< Integer, Integer, Integer >
{
  
  /////////////////////////////////////////////////////////////////////////////
  
  public static final int CONSTANT_CODE_FRAGMENT_ELEMENT_EMPTY_ID = -1;
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectCodeFragmentElement( int fileIndex, int localLineIndex, int globalLineIndex )
  {
    super( fileIndex, localLineIndex, globalLineIndex );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectCodeFragmentElement( ObjectCodeFragmentElement objectCodeFragmentElement )
  {
    super( objectCodeFragmentElement.first, objectCodeFragmentElement.second, objectCodeFragmentElement.third );
  }
  
  /////////////////////////////////////////////////////////////////////////////
}