package pre.populator;

import java.util.List;

import core.individual.IIndividual;

public interface IPopulator {

	List<IIndividual> getInitialPopulation();

}
