package pre.populator;

import java.util.ArrayList;
import java.util.List;

import core.individual.IIndividual;
import core.individual.IndividualText;

public class PopulatorGoldTrucks implements IPopulator {

	@Override
	public List<IIndividual> getInitialPopulation() {

		ArrayList<IIndividual> individuals = new ArrayList<IIndividual>();
		
		IndividualText first = new IndividualText("shipment of gold damaged in a fire");
		IndividualText second = new IndividualText("delivery of silver arrived in a silver truck");
		IndividualText third = new IndividualText("shipment of gold arrived in a truck");
		
		individuals.add(first);
		individuals.add(second);
		individuals.add(third);
		
		return individuals;
	}

}
