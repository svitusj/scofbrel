package pre.preprocessor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import core.Query;
import core.individual.IIndividual;
import core.individual.IndividualText;
import core.population.IPopulation;
import opennlp.tools.lemmatizer.SimpleLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.WhitespaceTokenizer;

public class PreprocessorNLP implements IPreprocessor {

	
	@Override
	public void process(IPopulation population) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void process(IPopulation population, Query query) {
		
		// Process the query
		basicProcessing(query);
		posTag(query);
		lemmatize(query);
		nounFiltering(query);
		
		// Process the individuals
		for(IIndividual individual : population.getPopulation()){
			basicProcessing(individual);
			posTag(individual);
			lemmatize(individual);
			nounFiltering(individual);
		}
		
	}
	
	/* Variables for the linguistic models */
	private final String posModelRoute = "/resources/en-pos-maxent.bin";
	private final String dictRoute = "/resources/en-lemmatizer.dict";
	
	/* Automatic Linguistic Preprocessing methods */
	
	private void basicProcessing(IIndividual individual)
	{
		
		IndividualText indText = (IndividualText) individual;
		
		// Tokenize the string
        String[] individualWords = WhitespaceTokenizer.INSTANCE.tokenize(indText.getText());
                
        ArrayList<String> processedInd = new ArrayList<String>();

        // Add the words to the individual processed list
        for(String word : individualWords)
        {
        	// Lowercase the word first
        	word = word.toLowerCase();
        	processedInd.add(word);
        }
        
        indText.setProcessedText(processedInd);
        
	}
	
	private void basicProcessing(Query query) {
		
		String[] queryWords = WhitespaceTokenizer.INSTANCE.tokenize(query.getOriginalQuery());
        ArrayList<String> processedQuery = new ArrayList<String>();
        
        // Add the words to the individual processed list
        for(String word : queryWords)
        {
        	// Lowercase the word first
        	word = word.toLowerCase();
        	processedQuery.add(word);
        }
        
        query.setProcessedQuery(processedQuery);
	}
	
	@SuppressWarnings("deprecation")
	private void posTag(IIndividual individual)
    {
        try 
        {
        	/* START POS TAGGING */
            
        	// Create the POS Model from resource stream
            InputStream is = this.getClass().getResourceAsStream(posModelRoute);
            final POSModel model = new POSModel(is);
          
            // Create the POS tagger from the model
            POSTaggerME tagger = new POSTaggerME(model);
            
            // Close the resource stream
            is.close();
            
            // Cast individual
            IndividualText indText = (IndividualText) individual; 
            
            // Get the words as array
            ArrayList<String> individualWords = indText.getProcessedText();
                       
            // Get the array of tags
            List<String> indT = tagger.tag(individualWords);
            
            ArrayList<String> indTags = new ArrayList<String>();
            indTags.addAll(indT);
            
            // Store the tags in the proper token variable
            indText.setPosTags(indTags);
            
            /* END POS TAGGING */
            
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }
	
	@SuppressWarnings("deprecation")
	private void posTag(Query query)
    {
        try 
        {
        	/* START POS TAGGING */
            
        	// Create the POS Model from resource stream
            InputStream is = this.getClass().getResourceAsStream(posModelRoute);
            final POSModel model = new POSModel(is);
          
            // Create the POS tagger from the model
            POSTaggerME tagger = new POSTaggerME(model);
            
            // Close the resource stream
            is.close();
                       
            // Get the words as array
            ArrayList<String> queryWords = query.getProcessedQuery();
                       
            // Get the array of tags
            List<String> queryT = tagger.tag(queryWords);
            ArrayList<String> queryTags = new ArrayList<String>();
            queryTags.addAll(queryT);
            
            // Store the tags in the proper token variable
            query.setPosTags(queryTags);
            
            /* END POS TAGGING */
            
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }

	private void lemmatize(IIndividual individual) 
	{
		try
		{
			// Get the dictionary as stream
			InputStream is = this.getClass().getResourceAsStream(dictRoute);
			// Create the lemmatizer
			SimpleLemmatizer lemmatizer = new SimpleLemmatizer(is);
			// Close the stream
			is.close();
			
			IndividualText indText = (IndividualText) individual;
			
			// Get the words as array
            ArrayList<String> words = indText.getProcessedText();
                       
            // Get the array of tags
			ArrayList<String> tags = indText.getPosTags();
			
			// Create the array of lemmas
			ArrayList<String> lemmas = new ArrayList<String>();
			int index = 0;
			
			// For all the words of the processed individual
			for(String word : words)
			{
				// Lemmatize word according to its postag
				String lemma = lemmatizer.lemmatize(word, tags.get(index));
				index++;
				
				// Save the lemma
				lemmas.add(lemma);
			}
			
			indText.setLemmas(lemmas);
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void lemmatize(Query query) 
	{
		try
		{
			// Get the dictionary as stream
			InputStream is = this.getClass().getResourceAsStream(dictRoute);
			// Create the lemmatizer
			SimpleLemmatizer lemmatizer = new SimpleLemmatizer(is);
			// Close the stream
			is.close();
			
			// Get the words as array
            ArrayList<String> words = query.getProcessedQuery();
                       
            // Get the array of tags
			ArrayList<String> tags = query.getPosTags();
			
			// Create the array of lemmas
			ArrayList<String> lemmas = new ArrayList<String>();
			int index = 0;
			
			// For all the words of the processed individual
			for(String word : words)
			{
				// Lemmatize word according to its postag
				String lemma = lemmatizer.lemmatize(word, tags.get(index));
				index++;
				
				// Save the lemma
				lemmas.add(lemma);
			}
			
			query.setLemmas(lemmas);
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void nounFiltering(IIndividual individual)
	{
		
		IndividualText indText = (IndividualText) individual;
		ArrayList<String> filteredWords = new ArrayList<String>();
		ArrayList<String> filteredTags = new ArrayList<String>();
		ArrayList<String> filteredLemmas = new ArrayList<String>();
		Set<String> filteredTerms = new LinkedHashSet<String>();
		
		
		int index = 0;
		
		// Form the processed string by adding only the nouns
		for (String word : indText.getProcessedText()) {
			if (indText.getPosTags().get(index).equals("NN")) {
				filteredWords.add(word);
				filteredTags.add(indText.getPosTags().get(index));
				filteredLemmas.add(indText.getLemmas().get(index));
			}
			index++;
		}
		
		filteredTerms.addAll(filteredWords);
		
		indText.setProcessedText(filteredWords);
		indText.setPosTags(filteredTags);
		indText.setLemmas(filteredLemmas);
		indText.setTerms(filteredTerms);
	}
	
	private void nounFiltering(Query query)
	{
		
		ArrayList<String> filteredWords = new ArrayList<String>();
		ArrayList<String> filteredTags = new ArrayList<String>();
		ArrayList<String> filteredLemmas = new ArrayList<String>();
		Set<String> filteredTerms = new LinkedHashSet<String>();
				
		int index = 0;
		
		// Form the processed string by adding only the nouns
		for (String word : query.getProcessedQuery()) {
			if (query.getPosTags().get(index).equals("NN")) {
				filteredWords.add(word);
				filteredTags.add(query.getPosTags().get(index));
				filteredLemmas.add(query.getLemmas().get(index));
			}
			index++;
		}
		
		filteredTerms.addAll(filteredWords);
		
		query.setProcessedQuery(filteredWords);
		query.setPosTags(filteredTags);
		query.setLemmas(filteredLemmas);
		query.setTerms(filteredTerms);
	}


}
