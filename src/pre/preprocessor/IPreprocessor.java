package pre.preprocessor;

import core.Query;
import core.population.IPopulation;

public interface IPreprocessor {

	public void process(IPopulation population);
	
	public void process(IPopulation population, Query query);

}
