import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
// Flimea management
import java.util.LinkedHashSet;
import core.individual.IIndividual;
import core.individual.IndividualText;
import core.population.IPopulation;
import core.population.Population;
import core.Query;
import ea.context.Context;
import ea.fitness.LSIFitness;
import ea.operator.crossover.MaskCrossover;
import ea.operator.mutation.RandomMutation;
import ea.operator.replace.ElitismReplace;
import ea.operator.selection.RouletteWheelSelection;
import pre.populator.PopulatorGoldTrucks;
//import pre.preprocessor.PreprocessorNLP;

public class ObjectFitnessStore
{
  /////////////////////////////////////////////////////////////////////////////
  
  public static final boolean CONSTANT_IS_EXTERNAL_FITNESS_USED  = true;
  
  public static final boolean CONSTANT_IS_PRESENCE_BONUS_APPLIED = false;

  public static final double  CONSTANT_FITNESS_TARGET_THRESHOLD  = 0.95f;
  
  /////////////////////////////////////////////////////////////////////////////
  
  private ArrayList< ObjectQueryData > mQueries;    ///< Query list
  
  private Map< String, Integer >       mQueryWords; ///< Query word list

  /////////////////////////////////////////////////////////////////////////////

  public ObjectFitnessStore()
  {
    mQueries = new ArrayList< ObjectQueryData >();
    mQueryWords = new HashMap<>();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public int AddQueryAndReturnIndex( ObjectQueryData queryData )
  {
    mQueries.add( queryData );
    for ( int i = 0; i < queryData.size(); ++i ) {
      if ( mQueryWords.get( queryData.get( i ) ) == null ) {
        mQueryWords.put( queryData.get( i ), mQueryWords.size() );
      }
    }
    return mQueries.size() - 1;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void ListQueryWords()
  {
    System.out.print(
        ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE + 
        "Query Words: " +
        ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE );
    
    
    String wordIndexStringFormat = "%0" + ( ( int )Math.log10( mQueryWords.size() ) + 1 ) + "d";
    mQueryWords.forEach
    (
      ( word, wordIndex ) ->
      {
        System.out.println( "Index: " + String.format( wordIndexStringFormat, wordIndex ) + " Word: " + word );
      }
    );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void UpdateWordCountInQueries()
  {
    for ( int i = 0; i < mQueries.size(); ++i ) {
      
      if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE ) {
        System.out.print(
            ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE + 
            "Query : " + String.format( "%0" + ( ( int )Math.log10( mQueries.size() ) + 1 ) + "d", i ) +
            " : " + mQueries.get( i ).toString() +
            ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE );
      }
      
      for ( int j = 0; j < mQueries.get( i ).size(); ++j ) {
        if ( mQueryWords.get( mQueries.get( i ).get( j ) ) != null ) {
          int index = mQueryWords.get( mQueries.get( i ).get( j ) );
          int counter = 1;
          if ( mQueries.get( i ).mWordIndexMatches.get( index ) != null ) {
            counter = 1 + mQueries.get( i ).mWordIndexMatches.get( index );
          }
          mQueries.get( i ).mWordIndexMatches.put( index, counter );
        }
      }
      
      if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE ) {
        String wordIndexStringFormat = "%0" + ( ( int )Math.log10( mQueryWords.size() ) + 1 ) + "d";
        mQueries.get( i ).mWordIndexMatches.forEach
        (
          ( wordIndex, count ) ->
          {
            System.out.println( "Word Index: " + String.format( wordIndexStringFormat, wordIndex ) + " Count: " + count );
          }
        );
      }
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public Map< Integer, Integer> GetWordCountInWordSet( ObjectWordSet words )
  {
    Map< Integer, Integer > wordIndexMatches = new HashMap<>();
    
    for ( int i = 0; i < words.size(); ++i ) {
      if ( mQueryWords.get( words.get( i ) ) != null ) {
        int index = mQueryWords.get( words.get( i ) );
        int counter = 1;
        if ( wordIndexMatches.get( index ) != null ) {
          counter = 1 + wordIndexMatches.get( index );
        }
        wordIndexMatches.put( index, counter );
      }
    }
    
    if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE ) {
      String wordIndexStringFormat = "%0" + ( ( int )Math.log10( mQueryWords.size() ) + 1 ) + "d";
      wordIndexMatches.forEach
      (
        ( wordIndex, count ) ->
        {
          System.out.println( "Word Index: " + String.format( wordIndexStringFormat, wordIndex ) + " Count: " + count );
        }
      );
    }
    
    return wordIndexMatches;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public double CalculateFitness( int queryIndex, int codeFragmentIndex, ObjectWordSet codeFragmentWords )
  {
    double fitnessValue = 0.0f;
    
    if ( queryIndex < mQueries.size() ) {
      
      if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE && ObjectSCoFBReL.CONSTANT_IS_CODE_FITNESS_PROCESS_SHOWN ) {
        System.out.print(
            ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE + 
            "Code Fragment: " + codeFragmentIndex + " : " + codeFragmentWords.toString() +
            ObjectParser.CONSTANT_VERBOSE_NEW_FILE_LINE );
      }
      
      if ( CONSTANT_IS_EXTERNAL_FITNESS_USED ) {
        // External fitness
        
        // Population
        ArrayList<IIndividual> individuals = new ArrayList<IIndividual>();
        IndividualText individual = new IndividualText( "Individual" );
        Set< String > codeFragmentTerms = new LinkedHashSet< String >();
        codeFragmentTerms.addAll( codeFragmentWords );
        individual.setTerms( codeFragmentTerms );
        individual.setProcessedText( codeFragmentWords );
        individuals.add( individual );
        Population inputPopulation = new Population();
        inputPopulation.setInitialPopulation( individuals );
        // Query
        Query query = new Query( "Query" );
        Set< String > queryTerms = new LinkedHashSet< String >();
        queryTerms.addAll( mQueries.get( queryIndex ) );
        query.setTerms( queryTerms );
        query.setProcessedQuery( mQueries.get( queryIndex ) );
        // LSI
        LSIFitness lsiFitness = new LSIFitness();
        lsiFitness.setQuery( query );
        // Context
        Context context = new Context(
            inputPopulation,
            new PopulatorGoldTrucks(),
            new MaskCrossover(),
            new RandomMutation(),
            new RouletteWheelSelection(),
            new ElitismReplace(),
            lsiFitness,
            null/*new List<IPreprocessor>*/ );
        // Fitness calculation
        IPopulation outputPopulation = lsiFitness.assess( context );
        if ( outputPopulation.getPopulation().size() == 1 ) {
          fitnessValue =  outputPopulation.getPopulation().get( 0 ).getFitness();
        }
        else {
          System.out.println( "Just one element expected. Number of elements: " + outputPopulation.getPopulation().size() );
        }
      }
      else {
        // Custom fitness
        
        Set< Entry< Integer, Integer > > SetqueryWordIndexMatches = mQueries.get( queryIndex ).mWordIndexMatches.entrySet();
        Iterator< Entry< Integer, Integer > > iteratorQuery = SetqueryWordIndexMatches.iterator();
        
        Map< Integer, Integer > codeFragmentWordIndexMatches = GetWordCountInWordSet( codeFragmentWords );
        Set< Entry< Integer, Integer > > SetCodeFragmentWordIndexMatches = codeFragmentWordIndexMatches.entrySet();
        Iterator< Entry< Integer, Integer > > iteratorCodeFragment = SetCodeFragmentWordIndexMatches.iterator();
        
        
        double querySumOfSquares = 0.0f;
        double queryModule = 0.0f;      
        
        double codeFragmentSumOfSquares = 0.0f;
        double codeFragmentModule = 0.0f;
        
        double dotProductValue = 0.0f;
        
        while( iteratorQuery.hasNext() ) {
          Entry< Integer, Integer > entryQuery = iteratorQuery.next();
          if ( codeFragmentWordIndexMatches.get( entryQuery.getKey() ) != null ) {
            dotProductValue += (double)( entryQuery.getValue() * codeFragmentWordIndexMatches.get( entryQuery.getKey() ) );
          }
          querySumOfSquares += Math.pow(  entryQuery.getValue(), 2.0f );
        }
        queryModule = Math.sqrt( querySumOfSquares );
        
        
        while( iteratorCodeFragment.hasNext() ) {
          Entry< Integer, Integer > entryCodeFragment = iteratorCodeFragment.next();
          codeFragmentSumOfSquares += Math.pow(  entryCodeFragment.getValue(), 2.0f );
        }
        codeFragmentModule = Math.sqrt( codeFragmentSumOfSquares );
  
        fitnessValue = dotProductValue / ( queryModule * codeFragmentModule );
      }
      
      

      
      mQueries.get( queryIndex ).mCodeFragmentFitnesBatches.put( codeFragmentIndex, fitnessValue );
      
      if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE && ObjectSCoFBReL.CONSTANT_IS_CODE_FITNESS_PROCESS_SHOWN ) {
        System.out.println(
            "Query: " + queryIndex + ObjectParser.CONSTANT_NEW_LINE_STRING +
            "Code Fragment: " + codeFragmentIndex +
            "Fitness Value: " + fitnessValue );
      }
    }
    
    return fitnessValue;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ArrayList<Double> CalculateFitness( int queryIndex, ArrayList<Integer> codeFragmentIndices, ArrayList<ObjectWordSet> codeFragmentWordSets )
  {
    ArrayList<Double> fitnessValues = new ArrayList<Double>();
    
    if ( queryIndex < mQueries.size() ) {

      if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE && ObjectSCoFBReL.CONSTANT_IS_CODE_FITNESS_PROCESS_SHOWN ) {
        System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + "Calculating fitness for " + codeFragmentIndices.size() + " code fragments" );
      }
      
      if ( CONSTANT_IS_EXTERNAL_FITNESS_USED ) {
        // External fitness
        
        // Query
        Query query = new Query( "Query" );
        Set< String > queryTerms = new LinkedHashSet< String >();
        queryTerms.addAll( mQueries.get( queryIndex ) );
        query.setTerms( queryTerms );
        query.setProcessedQuery( mQueries.get( queryIndex ) );
     
        // Presence bonus
        ArrayList< Double > presenceBonusList = new ArrayList< Double >();
        
        // Population
        ArrayList<IIndividual> individuals = new ArrayList<IIndividual>();
        for ( int h = 0; h < codeFragmentIndices.size(); ++h ) {
          IndividualText individual = new IndividualText( "Individual" );
          Set< String > codeFragmentTerms = new LinkedHashSet< String >();
          codeFragmentTerms.addAll( codeFragmentWordSets.get( h ) );
          individual.setTerms( codeFragmentTerms );
          individual.setProcessedText( codeFragmentWordSets.get( h ) );
          individuals.add( individual );
          
          if ( CONSTANT_IS_PRESENCE_BONUS_APPLIED ) {
            int termFoundCount = 0;
            for ( String s : queryTerms ) {
              if ( codeFragmentWordSets.get( h ).contains( s ) ) {
                ++termFoundCount;
              }
            }

            double factor = (double)termFoundCount / (double)queryTerms.size();
            double bonusFactor = ( factor >= 0.75f ) ? ( 0.3f * Math.pow( (double)termFoundCount / (double)queryTerms.size(), (double)4.0f ) ) : -0.3f;
            presenceBonusList.add( bonusFactor );
          }
          
          
          
          if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE && ObjectSCoFBReL.CONSTANT_IS_CODE_FITNESS_PROCESS_SHOWN ) {
            System.out.println( "Code Fragment: " + codeFragmentIndices.get( h ) + ": " + codeFragmentTerms.toString() );
          }
        }
        
        Population inputPopulation = new Population();
        inputPopulation.setInitialPopulation( individuals );

        // LSI
        LSIFitness lsiFitness = new LSIFitness();
        lsiFitness.setQuery( query );
        // Context
        Context context = new Context(
            inputPopulation,
            new PopulatorGoldTrucks(),
            new MaskCrossover(),
            new RandomMutation(),
            new RouletteWheelSelection(),
            new ElitismReplace(),
            lsiFitness,
            null/*new List<IPreprocessor>*/ );
        // Fitness calculation
        IPopulation outputPopulation = lsiFitness.assess( context );
        for ( int i = 0; i < outputPopulation.size(); ++i ) {
          double fitnessValue = outputPopulation.getPopulation().get( i ).getFitness() + ( CONSTANT_IS_PRESENCE_BONUS_APPLIED ? presenceBonusList.get( i ) : (double)0.0f );
          fitnessValue = Double.isNaN( fitnessValue ) ? -1.0f : fitnessValue;
          fitnessValues.add( fitnessValue );
        }
      }
      else {
        System.out.print( "Functionality not available." );
      }
      
      
      if ( codeFragmentIndices.size() != fitnessValues.size() ) {
        System.out.print( "Number of elements mismatch. Code fragments: " + codeFragmentIndices.size() + " Fitness values: " + fitnessValues.size() );
      }
      for ( int j = 0; j < fitnessValues.size(); ++j ) {
        mQueries.get( queryIndex ).mCodeFragmentFitnesBatches.put( codeFragmentIndices.get( j ), fitnessValues.get( j ) );
      }
      
      if ( ObjectSCoFBReL.CONSTANT_IS_VERBOSE && ObjectSCoFBReL.CONSTANT_IS_CODE_FITNESS_PROCESS_SHOWN ) {
        System.out.println( ObjectParser.CONSTANT_NEW_LINE_STRING + "Query index: " + queryIndex );
        for ( int g = 0; g < codeFragmentIndices.size(); ++g ) {
          System.out.println( "Code Fragment: " + codeFragmentIndices.get( g ) + " Fitness Value: " + fitnessValues.get( g ) );
        }
      }
    }
    
    return fitnessValues;
  }
  
  /////////////////////////////////////////////////////////////////////////////

}

