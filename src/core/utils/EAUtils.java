package core.utils;

import java.util.Random;

import core.individual.Genes;

public class EAUtils {

	static Random rng = new Random(System.currentTimeMillis());
	
	public static Genes generateRandomMask(int size) {
		
		Genes mask = new Genes(size);
		
		for (int i = 0; i < mask.size(); i++) {
			if (rng.nextFloat() < 0.5d) {
				mask.flip(i);
			}
		}
		
		return mask;
	}
	
	public static double getRandomDouble() {
		return rng.nextDouble();
	}

	public static int getRandomInt(int size) {
		return rng.nextInt(size);
	}

	public static float getRandomFloat() {
		return rng.nextFloat();
	}
}
