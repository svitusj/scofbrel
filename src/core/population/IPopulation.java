package core.population;

import java.util.List;

import core.individual.IIndividual;

public interface IPopulation {


	public void removeFromBottom(int amount);
	public void sortPopulationDescendant();
	public void addAll(List<IIndividual> offspring);
	public void setInitialPopulation(List<IIndividual> initialPopulation);
	public int size();
	public IIndividual get(int i);
	public List<IIndividual> getPopulation();
	
}
