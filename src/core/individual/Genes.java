package core.individual;

import java.util.BitSet;

public class Genes extends BitSet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Genes(int size) {
		super(size);
	}

	public void negate() {
		flip(0,size()-1);
	}

//	public String toString() {
//		String s = "";
//		for (int i = 0; i < elements.length; i++) {
//			if (elements[i])
//				s = 1 + s;
//			else
//				s = 0 + s;
//		}
//		return s;
//	}
	
	public Genes clone() {
		return (Genes) super.clone();
	}
	
}
