package core.individual;

import core.encoding.EMFToIntegerCODEC;

public interface IIndividual {

	public IIndividual clone();

	public Genes getGenes();
	public Genes getCopyOfGenes();
	public int getSize();

	public EMFToIntegerCODEC getCodec();

	public double getFitness();
	public void setFitness(double fitnessValue);

}
