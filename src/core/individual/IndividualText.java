package core.individual;

import java.util.ArrayList;
import java.util.Set;

import core.encoding.EMFToIntegerCODEC;

public class IndividualText implements IIndividual {

	// Text storage
	private String text;
	private ArrayList<String> processedText;
	private ArrayList<String> posTags;
	private ArrayList<String> lemmas;
	private Set<String> terms;
	
	// Fitness score
	private double fitness;

	// Constructor (using text)
	public IndividualText(String text) {
		this.text = text;
	}
	
	// Getters and setters
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public ArrayList<String> getProcessedText() {
		return processedText;
	}
	public void setProcessedText(ArrayList<String> processedText) {
		this.processedText = processedText;
	}
	public Set<String> getTerms() {
		return terms;
	}
	public void setTerms(Set<String> terms) {
		this.terms = terms;
	}
	@Override
	public void setFitness(double fitness) {
		this.fitness = fitness;
	}
	@Override
	public double getFitness() {
		return fitness;
	}
	public ArrayList<String> getPosTags() {
		return posTags;
	}
	public void setPosTags(ArrayList<String> posTags) {
		this.posTags = posTags;
	}
	public ArrayList<String> getLemmas() {
		return lemmas;
	}
	public void setLemmas(ArrayList<String> lemmas) {
		this.lemmas = lemmas;
	}

	// UNUSED MODEL METHODS
	@Override
	public IIndividual clone() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Genes getGenes() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Genes getCopyOfGenes() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public EMFToIntegerCODEC getCodec() {
		// TODO Auto-generated method stub
		return null;
	}

}
