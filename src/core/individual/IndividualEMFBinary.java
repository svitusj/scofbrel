package core.individual;

import core.encoding.EMFToIntegerCODEC;

public class IndividualEMFBinary implements IIndividual {

	private Genes genes;
	private double fitness;
	private EMFToIntegerCODEC codec;
	
	public IndividualEMFBinary(Genes bs, EMFToIntegerCODEC codec) {
		this.genes = bs;
		this.fitness=-1;
		this.codec = codec;
	}
	
	public IndividualEMFBinary(IIndividual individual) {
		this(individual.getGenes(), individual.getCodec());
		this.fitness = individual.getFitness();
	}

	public IndividualEMFBinary(EMFToIntegerCODEC codec) {
		this(new Genes(codec.getSize()),codec);
		this.fitness=-1;
	}
	
	public Genes getGenes() {
		return genes;
	}
	
	public void setCodec(EMFToIntegerCODEC codec) {
		this.codec = codec;
	}
	
	public Genes getCopyOfGenes() {
		return genes.clone();
	}

	public void setGenes(Genes genes) {
		this.genes = genes;
	}

	public double getFitness() {
		return fitness;
	}

	public void setFitness(double fitness) {
		this.fitness = fitness;
	}

	@Override
	public IIndividual clone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return genes.size();
	}

	@Override
	public EMFToIntegerCODEC getCodec() {
		// TODO Auto-generated method stub
		return codec;
	}
}
