package core;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class Query {

	private String originalQuery;
	private ArrayList<String> processedQuery;
	private ArrayList<String> posTags;
	private ArrayList<String> lemmas;
	private Set<String> terms;
	
	// GETTERS AND SETTERS
	public String getOriginalQuery() {
		return originalQuery;
	}
	public void setOriginalQuery(String originalQuery) {
		this.originalQuery = originalQuery;
	}
	public ArrayList<String> getProcessedQuery() {
		return processedQuery;
	}
	public void setProcessedQuery(ArrayList<String> processedQuery) {
		this.processedQuery = processedQuery;
	}
	public ArrayList<String> getPosTags() {
		return posTags;
	}
	public void setPosTags(ArrayList<String> posTags) {
		this.posTags = posTags;
	}
	public ArrayList<String> getLemmas() {
		return lemmas;
	}
	public void setLemmas(ArrayList<String> lemmas) {
		this.lemmas = lemmas;
	}
	public Set<String> getTerms() {
		return terms;
	}
	public void setTerms(Set<String> terms) {
		this.terms = terms;
	}

	
	public Query(String query) {
		originalQuery = query;
		processedQuery = new ArrayList<String>();
		posTags = new ArrayList<String>();
		lemmas = new ArrayList<String>();
		terms = new LinkedHashSet<String>();
	}
	
}
