package core.encoding;

import org.eclipse.emf.ecore.EObject;

import core.individual.IIndividual;

public interface IEncodingEMF {

	public IIndividual encode(EObject root);
	
	public EObject decode(IIndividual individual);
	
}
