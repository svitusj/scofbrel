package core.encoding;

import java.util.HashMap;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

public class EMFToIntegerCODEC  {

	private EObject root;	
	private HashMap<Integer, EObject> mapIDTOEOBject;
	private HashMap<EObject, Integer> mapEObjectToID;
	
	public EMFToIntegerCODEC(EObject root) {
		
		this.root = root;
		mapIDTOEOBject = new HashMap<Integer,EObject>();
		mapEObjectToID = new HashMap<EObject,Integer>();
		
		TreeIterator<EObject> elements = root.eAllContents();
		int index = 0;
		
		while (elements.hasNext()) {
			EObject object = elements.next();
			mapIDTOEOBject.put(index, object);
			
			mapEObjectToID.put(object, index);
			index++;
		}
	}
	
	public int getSize() {
		return mapIDTOEOBject.size();
	}

	public EObject getRoot() {
		return root;
	}
	
	public HashMap<Integer, EObject> getMapIDTOEOBject() {
		return mapIDTOEOBject;
	}

	public HashMap<EObject, Integer> getMapEObjectToID() {
		return mapEObjectToID;
	}
}
