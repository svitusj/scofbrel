package core.encoding;

import java.util.ArrayList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import core.individual.IndividualEMFBinary;
import core.individual.IIndividual;

public class EncodingEMFtoBinary implements IEncodingEMF {

	public IIndividual encode(EObject root) {
		EMFToIntegerCODEC codec = new EMFToIntegerCODEC(root);
		return new IndividualEMFBinary(codec);
	}

	public EObject decode(IIndividual individual) {
		
		IndividualEMFBinary auxIndividual = new IndividualEMFBinary(individual);
		
		EObject root = auxIndividual.getCodec().getRoot();
		TreeIterator<EObject> elements = EcoreUtil.getAllContents(root,false);
		
		ArrayList<EObject> toDelete = new ArrayList<EObject>();
		
		while (elements.hasNext()) {
			EObject object = elements.next();
			
			int index = individual.getCodec().getMapEObjectToID().get(object);
			if(!individual.getGenes().get(index)) {
				toDelete.add(object);
			}
		}
		
		for(EObject obj : toDelete) {
			EcoreUtil.delete(obj);
		}
		
		return auxIndividual.getCodec().getRoot();
	}
	
}


