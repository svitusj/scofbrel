public class ObjectUtils
{ 
  /////////////////////////////////////////////////////////////////////////////
  
  private static final ObjectDictionaryStore CONSTANT_DICTIONARY_STORE  = new ObjectDictionaryStore();
  
  private static final String                CONSTANT_DEFAULT_SEPARATOR = "[^a-zA-Z+]+";//"(^((a-zA-Z)+))"; ///< Default separator
  
  /////////////////////////////////////////////////////////////////////////////
  
  public static float GetClamped( float initialValue, float minimum, float maximum )
  {
    return Math.max( minimum, Math.min( maximum, initialValue ) );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public static ObjectWordSet GetTokenizedByCustomSeparator(
      String originalText,
      String separatorRegularExpression )
  {
    //([A-Z]+|[A-Z]?[a-z]+)(?=[A-Z]|\b)
    //(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])
    
    ObjectWordSet wordSet = new ObjectWordSet();
    String[] tokens = originalText.split( separatorRegularExpression );
    for ( int i = 0; i < tokens.length; ++i ) {
      for ( String word : tokens[i].split( "(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])" ) ) {
        word = word.toLowerCase();
        if ( CONSTANT_DICTIONARY_STORE.IsIndependentFromDictionary(
               word,
               ObjectDictionaryStore.LanguageType.LANGUAGE_TYPE_CPP ) ) {
          wordSet.add( word );
        }
      }
    }
    return wordSet;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public static ObjectWordSet GetTokenizedByDefaultSeparator( String originalText )
  {
    return GetTokenizedByCustomSeparator( originalText, CONSTANT_DEFAULT_SEPARATOR );
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public static void NoOp()
  {
  }
  
  /////////////////////////////////////////////////////////////////////////////
}