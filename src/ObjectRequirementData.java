// List management required
import java.util.ArrayList;

public class ObjectRequirementData
{ 
  /////////////////////////////////////////////////////////////////////////////
  
  private String               mRequirementLabel;       ///< Requirement label
  
  private String               mRequirementDescription; ///< Requirement description
  
  private ArrayList< Integer > mRequirementLineIndices; ///< Requirement line index list
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectRequirementData()
  {
    mRequirementLabel = "";
    mRequirementDescription = "";
    mRequirementLineIndices = new ArrayList< Integer >();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public String GetRequirementLabel()
  {
    return mRequirementLabel;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void SetRequirementLabel( String requirementLabel )
  {
    mRequirementLabel = requirementLabel;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void SetRequirementDescription( String requirementDescription )
  {
    mRequirementDescription = requirementDescription;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public String GetRequirementDescription()
  {
    return mRequirementDescription;
  }
 
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< Integer > GetLineIndices()
  {
    return mRequirementLineIndices;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void AddLineIndex( Integer lineIndex )
  {
    mRequirementLineIndices.add( lineIndex );
  }
  
  /////////////////////////////////////////////////////////////////////////////
}