// List management required
import java.util.ArrayList;

public class ObjectOptionData
{ 
  /////////////////////////////////////////////////////////////////////////////
  
  private String               mOptionLabel;       ///< Requirement label
  
  private String               mOptionDescription; ///< Requirement description
  
  private ArrayList< Integer > mOptionLineIndices; ///< Requirement line index list
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectOptionData()
  {
    mOptionLabel = "";
    mOptionDescription = "";
    mOptionLineIndices = new ArrayList< Integer >();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public String GetOptionLabel()
  {
    return mOptionLabel;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void SetOptionLabel( String optionLabel )
  {
    mOptionLabel = optionLabel;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void SetOptionDescription( String optionDescription )
  {
    mOptionDescription = optionDescription;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public String GetOptionDescription()
  {
    return mOptionDescription;
  }
 
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< Integer > GetLineIndices()
  {
    return mOptionLineIndices;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void AddLineIndex( Integer lineIndex )
  {
    mOptionLineIndices.add( lineIndex );
  }
  
  /////////////////////////////////////////////////////////////////////////////
}