// Array lists required
import java.util.ArrayList;

public class ObjectOptionStore
{
  /////////////////////////////////////////////////////////////////////////////
  
  private ArrayList< ObjectOptionData > mOptions; ///< Option list

  /////////////////////////////////////////////////////////////////////////////

  public ObjectOptionStore()
  {
    mOptions = new ArrayList< ObjectOptionData >();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public int GetNumberOfOptions()
  {
    return mOptions.size();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  ArrayList< ObjectOptionData > GetOptions()
  {
    return mOptions;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectOptionData GetOptionByLabel( String optionLabel )
  {
    ObjectOptionData objectOptionData= null;
    for ( int i = 0; i < mOptions.size(); ++i ) {
      if ( mOptions.get( i ).GetOptionLabel().equals( optionLabel ) ) {
        objectOptionData = mOptions.get( i );
      }
    }
    return objectOptionData;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void AddOption( ObjectOptionData objectOptionData )
  {
    mOptions.add( objectOptionData );
  }
 
  /////////////////////////////////////////////////////////////////////////////
  
  public void AddOption(
      String optionLabel,
      String optionDescription,
      ArrayList< Integer > optionLineIndices )
  {
    ObjectOptionData objectOptionData = new ObjectOptionData();
    AddOption( objectOptionData );
  }
  
  /////////////////////////////////////////////////////////////////////////////
}

