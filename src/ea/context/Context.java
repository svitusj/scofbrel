package ea.context;

import java.util.List;

import core.population.IPopulation;
import ea.fitness.IFitness;
import ea.operator.crossover.ICrossover;
import ea.operator.mutation.IMutation;
import ea.operator.replace.IReplace;
import ea.operator.selection.ISelection;
import pre.populator.IPopulator;
import pre.preprocessor.IPreprocessor;

public class Context extends BaseContext {

	public Context(IPopulation population, IPopulator populator, ICrossover crossover,
			IMutation mutation, ISelection parentSelection, IReplace replacement, IFitness fitness,
			List<IPreprocessor> preprocessors) {
		super();
		this.population = population;
		this.populator = populator;
		this.crossover = crossover;
		this.mutation = mutation;
		this.parentSelection = parentSelection;
		this.replacement = replacement;
		this.fitness = fitness;
		this.preprocessors = preprocessors;
		
		MAX_GENERATIONS = 100;
	}

	@Override
	public boolean stopConditionMeet() {
		// TODO Auto-generated method stub
		return generations>=MAX_GENERATIONS;
	}
	
}
