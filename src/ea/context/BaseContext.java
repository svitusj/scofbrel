package ea.context;

import java.util.List;

import core.individual.IIndividual;
import core.population.IPopulation;
import ea.fitness.IFitness;
import ea.operator.crossover.ICrossover;
import ea.operator.mutation.IMutation;
import ea.operator.replace.IReplace;
import ea.operator.selection.ISelection;
import pre.populator.IPopulator;
import pre.preprocessor.IPreprocessor;

public abstract class BaseContext {
	
	public int MAX_GENERATIONS = 100;
	public float CROSSOVER_PROBABILITY=1;
	public double MUTATION_PROBABILITY=1;
	
	
	public IPopulation population;
	public IPopulator populator;
	
	public ICrossover crossover;
	public IMutation mutation;
	public ISelection parentSelection;
	public IReplace replacement;
	
	public IFitness fitness;
	public List<IPreprocessor> preprocessors;
	public abstract boolean stopConditionMeet();
	
	public List<IIndividual> offspring;
	
	public int generations=0;


	
}
