package ea;

import ea.context.Context;
import pre.preprocessor.IPreprocessor;

public class BaseEA implements IEvolutionaryAlgorithm {

	
	Context context;
	
	@Override
	public void init() {
		
		context = new Context(null, null, null, null, null, null, null, null);
		
	}
	
	@Override
	public void preloop() {
		context.population.setInitialPopulation(context.populator.getInitialPopulation());
		
		for(IPreprocessor preprocessor : context.preprocessors) {
			preprocessor.process(context.population);
		}
	}
	
	@Override
	public void loop() {
			
			context.fitness.assess(context);
			
			if(context.stopConditionMeet())
				return;
			
			context.parentSelection.selectParents(context);
			context.crossover.crossover(context);
			context.mutation.mutate(context);
			context.replacement.replace(context);
			loop();
	}
	
	@Override
	public void postLoop() {
		
	}
}
