package ea;

public interface IEvolutionaryAlgorithm {

	void postLoop();

	void loop();

	void preloop();

	void init();

}
