package ea;

import java.util.List;

import core.Query;
import core.population.IPopulation;
import ea.context.Context;
import ea.fitness.IFitness;
import pre.preprocessor.IPreprocessor;

public class NoEA implements IEvolutionaryAlgorithm {

	// Variables for the Algorithm
	private Context context;
	private IPopulation population;
	private Query query;
	private IFitness fitnessFunction;
	private List<IPreprocessor> preprocessors;
	
	// Setters and getters	
	public IPopulation getPopulation() {
		return population;
	}
	public void setPopulation(IPopulation population) {
		this.population = population;
	}
	public Query getQuery() {
		return query;
	}
	public void setQuery(Query query) {
		this.query = query;
	}
	public IFitness getFitnessFunction() {
		return fitnessFunction;
	}
	public void setFitnessFunction(IFitness fitnessFunction) {
		this.fitnessFunction = fitnessFunction;
	}
	public List<IPreprocessor> getPreprocessors() {
		return preprocessors;
	}
	public void setPreprocessors(List<IPreprocessor> preprocessors) {
		this.preprocessors = preprocessors;
	}

	@Override
	public void init() {
		context = new Context(population, null, null, null, null, null, fitnessFunction, preprocessors);
	}
	
	
	@Override
	public void preloop() {
		// Processing
		for(IPreprocessor processor : context.preprocessors) {
			processor.process(context.population, query);
		}
	}
	
	@Override
	public void loop() {
		// Assess the population
		context.fitness.assess(context);
	}
	
	@Override
	public void postLoop() {
		// TODO: Info de resultados
	}

}