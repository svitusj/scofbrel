package ea.fitness;

import core.individual.IIndividual;
import core.population.IPopulation;
import core.utils.EAUtils;
import ea.context.BaseContext;

public class SizeFitness implements IFitness {

	@Override
	public IPopulation assess(BaseContext context) {
		
		for(IIndividual individual : context.population.getPopulation()) {
			individual.setFitness(EAUtils.getRandomFloat());
		}
		return context.population;
	}

}
