package ea.fitness;

import core.population.IPopulation;
import ea.context.BaseContext;

public interface IFitness {

	/**
	 * Modifies the population updating the fitness for each individual
	 * @param context of the EA
	 * @return the population modified for testing purposes (context.population has been modified)
	 */
	public IPopulation assess(BaseContext context);
}
