package ea.operator.replace;

import core.population.IPopulation;
import ea.context.BaseContext;

public class ElitismReplace implements IReplace {

	@Override
	public IPopulation replace(BaseContext context) {
		
		context.population.removeFromBottom(context.offspring.size());
		context.population.addAll(context.offspring);

		return context.population;
	}

	

}
