package ea.operator.replace;

import core.population.IPopulation;
import ea.context.BaseContext;

public interface IReplace {

	public IPopulation replace(BaseContext context);
}
