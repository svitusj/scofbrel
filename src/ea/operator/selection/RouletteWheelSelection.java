package ea.operator.selection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import core.individual.IIndividual;
import core.utils.EAUtils;
import ea.context.BaseContext;

public class RouletteWheelSelection implements ISelection {

	@Override
	public List<IIndividual> selectParents(BaseContext context) {
		
		int size=2;
		
		double[] cumulativeFitnesses = new double[context.population.size()];
	     cumulativeFitnesses[0] = context.population.get(0).getFitness();
		
	    for (int i = 1; i < context.population.size(); i++)
        {
            double fitness = context.population.get(i).getFitness();
            cumulativeFitnesses[i] = cumulativeFitnesses[i - 1] + fitness;
        }
	     
	    ArrayList<IIndividual> selection = new ArrayList<IIndividual>(size);
        for (int i = 0; i < size; i++)
        {
            double randomFitness = EAUtils.getRandomDouble() * cumulativeFitnesses[cumulativeFitnesses.length - 1];
            int index = Arrays.binarySearch(cumulativeFitnesses, randomFitness);
            if (index < 0)
            {
                index = Math.abs(index + 1);
            }
            selection.add(context.population.get(index));
        }
        
        context.offspring = selection;
        return selection;
	}
	
}
