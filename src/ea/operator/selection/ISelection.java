package ea.operator.selection;

import java.util.List;

import core.individual.IIndividual;
import ea.context.BaseContext;

public interface ISelection {

	public List<IIndividual> selectParents(BaseContext context);
	
}
