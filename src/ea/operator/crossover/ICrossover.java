package ea.operator.crossover;

import java.util.List;

import core.individual.IIndividual;
import ea.context.BaseContext;

public interface ICrossover {

	/**
	 * This method performs a crossover of the two individuals provided in the {@link BaseContext#offspring} and stores them in the same place. It also returns the newly created elements for testing purposes.
	 * @param context A configuration holding the parameters that may be needed for the crossover.
	 * @return A list holding the new individuals (usually 2) or null if the crossover is not performed (usually because of the probability)
	 */
	List<IIndividual> crossover(BaseContext context);
	
	/**
	 * This method performs two crossovers (mask positive and mask negative) of the two individuals provided as input and returns them.
	 * @param parent1 first parent
	 * @param parent2 second parent
	 * @return a list with the two new individuals
	 */
	List<IIndividual> crossover(IIndividual parent1,IIndividual parent2);
}
