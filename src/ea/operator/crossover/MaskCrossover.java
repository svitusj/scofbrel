package ea.operator.crossover;

import java.util.ArrayList;
import java.util.List;

import core.individual.Genes;
import core.individual.IIndividual;
import core.individual.IndividualEMFBinary;
import core.utils.EAUtils;
import ea.context.BaseContext;

public class MaskCrossover implements ICrossover {
	
	public List<IIndividual> crossover(BaseContext context) {
		
		if(EAUtils.getRandomDouble() > context.CROSSOVER_PROBABILITY || context.offspring.size()<2)
			return null;
		
		IIndividual f1 = context.offspring.get(0);
		IIndividual f2 = context.offspring.get(1);
		
		List<IIndividual> result = crossover(f1,f2);
		
		context.offspring.clear();
		context.offspring.addAll(result);
		
		return result;
	}

	@Override
	public List<IIndividual> crossover(IIndividual f1, IIndividual f2) {
		
		List<IIndividual> result = new ArrayList<IIndividual>();
		
		Genes mask = EAUtils.generateRandomMask(f1.getSize());
		Genes noMask = (Genes) mask.clone();
		noMask.negate();
		Genes aux1 = f1.getCopyOfGenes();
		Genes aux2 = f2.getCopyOfGenes();
		Genes aux3 = f1.getCopyOfGenes();
		Genes aux4 = f2.getCopyOfGenes();
		aux1.and(mask);
		aux2.and(noMask);
		aux1.or(aux2);
		
		result.add(new IndividualEMFBinary(aux1,f1.getCodec()));
		
		aux3.and(noMask);
		aux4.and(mask);
		aux3.or(aux4);
		
		result.add(new IndividualEMFBinary(aux3,f1.getCodec()));
		
		return result;
	}

}

