package ea.operator.mutation;

import java.util.ArrayList;
import java.util.List;

import core.individual.IIndividual;
import core.utils.EAUtils;
import ea.context.BaseContext;

public class RandomMutation implements IMutation{

	
	/**
	 * same probability for each model element of being added/removed
	 * 
	 * @param BinaryIndividual
	 * @return Individual
	 */
	public IIndividual mutate(IIndividual individual,BaseContext context) {

		if(Math.random() > context.MUTATION_PROBABILITY)
			return individual;
		
		return mutate(individual);
	}

	@Override
	public List<IIndividual> mutate(BaseContext context) {
		
		ArrayList<IIndividual> result = new ArrayList<IIndividual>();
		for(IIndividual individual : context.offspring) {
			result.add(mutate(individual,context));
		}
		
		return result;
	}

	@Override
	public IIndividual mutate(IIndividual individual) {
		int index = EAUtils.getRandomInt(individual.getGenes().size());

		individual.getGenes().flip(index);

		return individual;
	}
	
	
}
