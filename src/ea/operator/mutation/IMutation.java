package ea.operator.mutation;

import java.util.List;

import core.individual.IIndividual;
import ea.context.BaseContext;

public interface IMutation {

	public List<IIndividual> mutate(BaseContext context);
	
	public IIndividual mutate (IIndividual single);
	
}
