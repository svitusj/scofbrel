// List management required
import java.util.ArrayList;

public class ObjectCodeFragment extends ArrayList< ObjectCodeFragmentElement >
{
  /////////////////////////////////////////////////////////////////////////////
  
  public static final boolean CONSTANT_IS_NEO_CROSSOVER_ENABLED                  = true;
  
  public static final int     CONSTANT_CODE_FRAGMENT_DEFAULT_SIZE                = CONSTANT_IS_NEO_CROSSOVER_ENABLED ? 1 : 100;
  
  public static final int     CONSTANT_CODE_FRAGMENT_INDEX_INVALID               = -1;
  
  public static final int     CONSTANT_CODE_FRAGMENT_LAST_ADDITION_INDEX_INVALID = -1;
  
  /////////////////////////////////////////////////////////////////////////////
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectCodeFragment()
  {
    super();
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void SetContent( ArrayList< ObjectCodeFragmentElement > codeFragmentElements )
  {
    clear();
    for ( int i = 0; i < codeFragmentElements.size(); ++i ) {
      add( new ObjectCodeFragmentElement( codeFragmentElements.get( i ).first, codeFragmentElements.get( i ).second, codeFragmentElements.get( i ).third ) );
    }
    
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public int AddLineFromFileAndReturnIndex( int fileIndex, int localLineIndex, int globalLineIndex )
  {
    int indexReturned = CONSTANT_CODE_FRAGMENT_LAST_ADDITION_INDEX_INVALID;
    
    boolean isNotFound = true;
    for ( int h = 0; isNotFound && h < size(); ++h ) {
      if ( get( h ).first == fileIndex &&
           get( h ).second == localLineIndex &&
           get( h ).third == globalLineIndex ) {
        isNotFound = false;
      }
    }
    
    if ( isNotFound ) {
      add( new ObjectCodeFragmentElement( fileIndex, localLineIndex, globalLineIndex ) );
      indexReturned = size() - 1;
    }
    else {
      System.out.println( "Line already present in code fragment!" );
    }
    return indexReturned;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void Sort()
  {
    sort
    (
      ( ObjectCodeFragmentElement e1, ObjectCodeFragmentElement e2 ) ->
      {
        if ( e1.third > e2.third )
          return 1;
        if ( e1.third < e2.third )
          return -1;
        return 0;
      }
    );
  }
  
  /////////////////////////////////////////////////////////////////////////////
}