// Map management required
import java.util.Map;
import java.util.HashMap;

public class ObjectQueryData extends ObjectWordSet
{   
  /////////////////////////////////////////////////////////////////////////////
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
  public Map< Integer, Double >  mCodeFragmentFitnesBatches;
  
  public Map< Integer, Integer > mWordIndexMatches;
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectQueryData( String originalText )
  {
    super( originalText );
    mCodeFragmentFitnesBatches = new HashMap<>();
    mWordIndexMatches = new HashMap<>();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectQueryData( ObjectWordSet words )
  {
    addAll( words );
    mCodeFragmentFitnesBatches = new HashMap<>();
    mWordIndexMatches = new HashMap<>();
  }
  
  /////////////////////////////////////////////////////////////////////////////
}

