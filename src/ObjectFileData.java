// List management required
import java.util.ArrayList;

public class ObjectFileData
{ 
  /////////////////////////////////////////////////////////////////////////////
  
  private String                                     mFileName;            /// File name
  
  private String                                     mOptionalDescription; /// Optional description
  
  private ArrayList< ObjectCodeLine >                mLines;               ///< File line list
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectFileData()
  {
    mFileName = "";
    mOptionalDescription = "";
    mLines = new ArrayList< ObjectCodeLine >();
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public String GetFileName()
  {
    return mFileName;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void SetFileName( String fileName )
  {
    mFileName = fileName;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public void SetOptionalDescription( String optionalDescription )
  {
    mOptionalDescription = optionalDescription;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public String GetOptionalDescription()
  {
    return mOptionalDescription;
  }
 
  
  /////////////////////////////////////////////////////////////////////////////

  public ArrayList< ObjectCodeLine > GetLines()
  {
    return mLines;
  }
  
  /////////////////////////////////////////////////////////////////////////////

  public ObjectCodeLine GetLine( int lineIndex )
  {
    ObjectCodeLine line = new ObjectCodeLine( 0, 0, "", new ObjectWordSet() );
    try {
      line = mLines.get( lineIndex );
    }
    catch( Exception exception ) {
      System.out.println( "Exception retrieving code line at position: " + lineIndex + ": " + exception );
    }
    return line;
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public void AddLine( String line )
  {
    mLines.add( new ObjectCodeLine( 0, mLines.size(), line, new ObjectWordSet( line ) ) );
  }
  
  /////////////////////////////////////////////////////////////////////////////
}
