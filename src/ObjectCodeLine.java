public class ObjectCodeLine extends ObjectQuartet< Integer, Integer, String, ObjectWordSet >
{
  /////////////////////////////////////////////////////////////////////////////

  public enum LineType
  {
    LINE_TYPE_NORMAL,
    LINE_TYPE_FUNCTION_STARTER,
    LINE_TYPE_FUNCTION_SEPARATOR,
    LINE_TYPE_CLASS_DECLARATION,
    LINE_TYPES
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  
  public static final String  CONSTANT_FUNCTION_STARTER   = "/**";//"inline";
  
  public static final String  CONSTANT_FUNCTION_SEPARATOR = "/**********************************************************************************************************************/";
  
  public static final String  CONSTANT_CLASS_DECLARATOR   = "class ";
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectCodeLine( int fileIndex, int localLineIndex, String text, ObjectWordSet words )
  {
    super( fileIndex, localLineIndex, text, words );
  }
  
  /////////////////////////////////////////////////////////////////////////////
  
  public LineType GetLineType()
  {
    return third.contains( CONSTANT_FUNCTION_SEPARATOR ) ?
           LineType.LINE_TYPE_FUNCTION_SEPARATOR :
           ( /*contains*/third.endsWith( CONSTANT_FUNCTION_STARTER ) ? LineType.LINE_TYPE_FUNCTION_STARTER : ( ( third.contains( CONSTANT_CLASS_DECLARATOR ) ) ? LineType.LINE_TYPE_CLASS_DECLARATION : LineType.LINE_TYPE_NORMAL ) );
  }
  
  /////////////////////////////////////////////////////////////////////////////
}
