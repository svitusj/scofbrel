public class ObjectQuartet< quartetA, quartetB, quartetC, quartetD >
{
  /////////////////////////////////////////////////////////////////////////////
  
  public quartetA first;
  public quartetB second;
  public quartetC third;
  public quartetD fourth;
  
  /////////////////////////////////////////////////////////////////////////////
  
  public ObjectQuartet( quartetA newFirst, quartetB newSecond, quartetC newThird, quartetD newFourth )
  {
    first = newFirst;
    second = newSecond;
    third = newThird;
    fourth = newFourth;
  }
  
  /////////////////////////////////////////////////////////////////////////////
}